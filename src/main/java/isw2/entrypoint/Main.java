package isw2.entrypoint;
/*
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import isw2.domain.incidencia.Incidencia;
import isw2.domain.procedimiento.Procedimiento;
import isw2.domain.producto.Producto;
import isw2.domain.tecnico.Tecnico;
import isw2.service.incidencia.IncidenciaService;
import isw2.service.incidencia.IncidenciaServiceImpl;
import isw2.service.procedimiento.ProcedimientoService;
import isw2.service.procedimiento.ProcedimientoServiceImpl;
import isw2.service.producto.ProductoService;
import isw2.service.producto.ProductoServiceImpl;
import isw2.service.tecnico.TecnicoService;
import isw2.service.tecnico.TecnicoServiceImpl;
*/
public class Main {

	public static void main(String[] args) {
		/*
		TecnicoService tecnicoService = new TecnicoServiceImpl();
		ProcedimientoService procedimientoService = new ProcedimientoServiceImpl();
		ProductoService productoService = new ProductoServiceImpl();
		IncidenciaService incidenciaService = new IncidenciaServiceImpl();

		Tecnico tecnico1 =	tecnicoService.darDeAltaTecnico("tecnico1",
				"tecnico1", "987654321");
		Tecnico tecnico2 = tecnicoService.darDeAltaTecnico("tecnico2",
				"tecnico2", "987654321");
		Tecnico tecnico3 = tecnicoService.darDeAltaTecnico("tecnico3",
				"tecnico3", "987654321");

		Procedimiento wc1 = procedimientoService.darDeAltaProcedimiento("WC1",
				"WC1", "WC1");
		Procedimiento wc2 = procedimientoService.darDeAltaProcedimiento("WC2",
				"WC2", "WC2");
		Procedimiento wc3 = procedimientoService.darDeAltaProcedimiento("WC3",
				"WC3", "WC3");
		Producto webCam = productoService.darDeAltaProducto("Web Cam",
				"Web Cam", "Descripcion de Web Cam");
		productoService.asignarProcedimiento(webCam, wc1);
		productoService.asignarProcedimiento(webCam, wc2);
		productoService.asignarProcedimiento(webCam, wc3);

		Procedimiento pd1 = procedimientoService.darDeAltaProcedimiento("PD1",
				"PD1", "PD1");
		Producto penDrive16 = productoService.darDeAltaProducto("P16GB",
				"Pen drive 16GB", "Descripcion de Pen drive 16GB");
		productoService.asignarProcedimiento(penDrive16, pd1);

		Producto penDrive32 = productoService.darDeAltaProducto("P32GB",
				"Pen drive 32GB", "Descripcion de Pen drive 32GB");
		productoService.asignarProcedimiento(penDrive32, pd1);

		Procedimiento m1 = procedimientoService.darDeAltaProcedimiento("M1",
				"M1", "M1");
		Procedimiento m2 = procedimientoService.darDeAltaProcedimiento("M2",
				"M2", "M2");
		Procedimiento m3 = procedimientoService.darDeAltaProcedimiento("M3",
				"M3", "M3");
		Procedimiento m4 = procedimientoService.darDeAltaProcedimiento("M4",
				"M4", "M4");
		Producto monitor24 = productoService.darDeAltaProducto("M24in",
				"Monitor 24in", "Descripcion de Monitor 24in");
		productoService.asignarProcedimiento(monitor24, m1);
		productoService.asignarProcedimiento(monitor24, m2);
		productoService.asignarProcedimiento(monitor24, m3);
		productoService.asignarProcedimiento(monitor24, m4);

		Producto monitor32 = productoService.darDeAltaProducto("M32in",
				"Monitor 32in", "Descripcion de Monitor 32in");
		productoService.asignarProcedimiento(monitor32, m1);
		productoService.asignarProcedimiento(monitor32, m2);
		productoService.asignarProcedimiento(monitor32, m3);

		tecnicoService.asignarProcedimiento(tecnico1, wc1);
		tecnicoService.asignarProcedimiento(tecnico1, wc2);
		tecnicoService.asignarProcedimiento(tecnico1, wc3);

		tecnicoService.asignarProcedimiento(tecnico2, wc1);
		tecnicoService.asignarProcedimiento(tecnico2, wc2);
		tecnicoService.asignarProcedimiento(tecnico2, wc3);
		tecnicoService.asignarProcedimiento(tecnico2, pd1);
		
		for (Procedimiento p : procedimientoService.listarProcedimientos()) {
			tecnicoService.asignarProcedimiento(tecnico3, p);
		}

		incidenciaService.registrarIncidencia(
				"benito", "benito@gmail.com", "Descripcion",
				new GregorianCalendar(), webCam, wc1,
				new LinkedList<Incidencia>());
		Incidencia incidencia2 = incidenciaService.registrarIncidencia(
				"benito", "benito@gmail.com", "Descripcion",
				new GregorianCalendar(), webCam, wc2,
				new LinkedList<Incidencia>());
		Incidencia incidencia3 = incidenciaService.registrarIncidencia(
				"benito", "benito@gmail.com", "Descripcion",
				new GregorianCalendar(), penDrive32, pd1,
				new LinkedList<Incidencia>());
		incidenciaService.registrarIncidencia("benito", "benito@gmail.com",
				"Descripcion", new GregorianCalendar(), monitor24, m1,
				new LinkedList<Incidencia>());
		incidenciaService.registrarIncidencia("benito", "benito@gmail.com",
				"Descripcion", new GregorianCalendar(), monitor32, null,
				new LinkedList<Incidencia>());

		Incidencia incidencia1 = incidenciaService.seleccionarIncidencia(1);
		
		tecnicoService.reclamarIncidencia(tecnico1, incidencia1);
		tecnicoService.reclamarIncidencia(tecnico1, incidencia2);
		tecnicoService.reclamarIncidencia(tecnico2, incidencia3);

		incidenciaService.resolver(incidencia1);

		incidenciaService.addRespuesta(incidencia1, "Respuesta", -5);

		List<Incidencia> incidenciasRel = new LinkedList<Incidencia>();
		incidenciasRel.add(incidencia1);
		Incidencia incidencia6 = incidenciaService.registrarIncidencia(
				"benito", "benito@gmail.com", "Descripcion",
				new GregorianCalendar(), webCam, wc1, incidenciasRel);

		tecnicoService.reclamarIncidencia(tecnico1, incidencia6);

		incidenciaService.resolver(incidencia6);
		incidenciaService.resolver(incidencia2);
		*/
	}

}
