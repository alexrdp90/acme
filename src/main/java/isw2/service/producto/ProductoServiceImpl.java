package isw2.service.producto;

import java.util.List;

import javax.persistence.EntityManager;

import isw2.infrastructure.ProductoRepository;
import isw2.presentation.shared.ProductoDTO;
import isw2.domain.procedimiento.Procedimiento;
import isw2.domain.procedimiento.ProcedimientoImpl;
import isw2.domain.producto.Producto;
import isw2.domain.producto.ProductoImpl;

public class ProductoServiceImpl implements ProductoService {

	private ProductoRepository repository;

	public ProductoServiceImpl() {
		repository = new ProductoRepository();
	}

	public Producto darDeAltaProducto(String codigo, String nombre,
			String descripcion) {
		Producto producto = new ProductoImpl(codigo, nombre, descripcion);
		repository.store(producto);
		return producto;
	}

	public List<Producto> listarProductos() {
		return repository.findAll();
	}

	public Producto seleccionarProducto(String id) {
		return repository.find(id);
	}

	public void asignarProcedimiento(String codigoProducto,
			String codigoProcedimiento) {
		EntityManager em = repository.getEntityManager();
		try{
			em.getTransaction().begin();
			Procedimiento procedimiento = em.find(ProcedimientoImpl.class, codigoProcedimiento);
			Producto producto = em.find(ProductoImpl.class, codigoProducto);
			producto.addProcedimiento(procedimiento);
			em.getTransaction().commit();
		} catch(Exception e){
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
	}
	

	public List<ProductoDTO> listarProcedimientosDTOs() {
		return repository.findAllDTOs();
	}
}
