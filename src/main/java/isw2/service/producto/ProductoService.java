package isw2.service.producto;

import java.util.List;
import isw2.domain.producto.Producto;
import isw2.presentation.shared.ProductoDTO;

public interface ProductoService {
	
	public Producto darDeAltaProducto(String codigo, String nombre, String descripcion);
	
	public void asignarProcedimiento(String codigoProducto, String codigoProcedimiento);
	
	public List<Producto> listarProductos();

	public Producto seleccionarProducto(String id);
	
	public List<ProductoDTO> listarProcedimientosDTOs();

}

