package isw2.service.procedimiento;

import java.util.List;
import isw2.domain.procedimiento.Procedimiento;
import isw2.presentation.shared.ProcedimientoDTO;

public interface ProcedimientoService {

	Procedimiento añadirProcedimiento(String codigo, String nombre, String descripcion);
	List<Procedimiento> listarProcedimientos();
	List<ProcedimientoDTO> listarProcedimientosDTOs();

	public void actualizarDatos(String codigo, String nombre, String descripcion);

	public Procedimiento seleccionarProcedimiento(String codigo);

	public List<ProcedimientoDTO> seleccionarProcedimientoTecnico(String name);
	
	public  List<ProcedimientoDTO> seleccionarProcedimientoProducto(String nombre);

}
