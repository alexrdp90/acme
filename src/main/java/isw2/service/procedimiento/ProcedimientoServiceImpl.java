package isw2.service.procedimiento;

import isw2.infrastructure.ProcedimientoDAO;
import isw2.presentation.shared.ProcedimientoDTO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import isw2.domain.procedimiento.*;

public class ProcedimientoServiceImpl implements ProcedimientoService {

	@PersistenceUnit
	private EntityManagerFactory emf;

	public List<ProcedimientoDTO> listarProcedimientosDTOs() {
		return repository.findAllDTOs();
	}
	
	// GOOD!
	public Procedimiento añadirProcedimiento(String codigo, String nombre,
			String descripcion) {
		EntityManager em = emf.createEntityManager();
		ProcedimientoDAO dao = new ProcedimientoDAO(em);
		Procedimiento procedimiento = null;
		try {
			em.getTransaction().begin();
			procedimiento = new ProcedimientoImpl(codigo, nombre, descripcion);
			dao.store(procedimiento);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return procedimiento;
	}

	// GOOD!
	public Procedimiento seleccionarProcedimiento(String codigo) {
		EntityManager em = emf.createEntityManager();
		ProcedimientoDAO dao = new ProcedimientoDAO(em);
		Procedimiento procedimiento = null;
		try {
			em.getTransaction().begin();
			procedimiento = dao.find(codigo);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return procedimiento;
	}
	
	public List<ProcedimientoDTO> seleccionarProcedimientoTecnico(String nombre) {
		return repository.findAllDTOs(nombre);
	}
	
	public List<ProcedimientoDTO> seleccionarProcedimientoProducto(String nombre) {
		return repository.findAllProdDTOs(nombre);
	}
	
	public List<Procedimiento> listarProcedimientos() {
		return repository.findAll();
	}

	public void actualizarDatos(String codigo, String nombre, String descripcion) {
		Procedimiento procedimiento = seleccionarProcedimiento(codigo);
		procedimiento.setNombre(nombre);
		procedimiento.setDescripcion(descripcion);
		repository.update(procedimiento);
	}

	

}
