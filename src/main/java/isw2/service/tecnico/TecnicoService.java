package isw2.service.tecnico;

import isw2.domain.tecnico.Tecnico;
import isw2.presentation.shared.TecnicoDTO;

import java.util.List;

public interface TecnicoService {
	
	List<TecnicoDTO> listarTecnicosDTOs();
	void registrarTecnicoSystem();
	Tecnico añadirTecnico(String nombre, String contraseña, String telefono, List<String> procedimientos);
	Tecnico seleccionarTecnico(Integer id);
	Tecnico seleccionarTecnicoPorNombre(String nombre);
	
	void asignarProcedimiento(Integer idTecnico, String codigoProcedimiento);	
	void reclamarIncidencia(Integer idTecnico, Integer idIncidencia);
	
}
