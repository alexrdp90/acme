package isw2.service.tecnico;

import isw2.domain.incidencia.Incidencia;
import isw2.domain.incidencia.IncidenciaImpl;
import isw2.domain.procedimiento.Procedimiento;
import isw2.domain.procedimiento.ProcedimientoImpl;
import isw2.domain.tecnico.Tecnico;
import isw2.domain.tecnico.TecnicoImpl;
import isw2.infrastructure.ProcedimientoDAO;
import isw2.infrastructure.TecnicoDAO;
import isw2.presentation.shared.TecnicoDTO;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceUnit;

public class TecnicoServiceImpl implements TecnicoService {

	@PersistenceUnit
	private EntityManagerFactory emf;

	public List<TecnicoDTO> listarTecnicosDTOs() {
		EntityManager em = emf.createEntityManager();
		TecnicoDAO dao = new TecnicoDAO(em);
		List<TecnicoDTO> tecnicos = new LinkedList<TecnicoDTO>();
		try {
			em.getTransaction().begin();
			tecnicos = dao.listarTecnicos();
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return tecnicos;
	}

	public void registrarTecnicoSystem() {
		EntityManager em = emf.createEntityManager();
		TecnicoDAO dao = new TecnicoDAO(em);
		Tecnico system = new TecnicoImpl("SYSTEM");
		try {
			em.getTransaction().begin();
			dao.añadirTecnico(system);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
	}

	public Tecnico añadirTecnico(String nombre, String contraseña,
			String telefono, List<String> procedimientos) {
		EntityManager em = emf.createEntityManager();
		TecnicoDAO tecnicoDAO = new TecnicoDAO(em);
		ProcedimientoDAO procedimientoDAO = new ProcedimientoDAO(em);
		Tecnico tecnico = null;
		try {
			em.getTransaction().begin();
			tecnico = new TecnicoImpl(nombre, contraseña, telefono);
			tecnicoDAO.añadirTecnico(tecnico);
			em.persist(tecnico);
			for (String codigo : procedimientos) {
				Procedimiento proc = procedimientoDAO.find(codigo);
				tecnicoDAO.asignarProcedimiento(tecnico.getId(), proc);
			}
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return tecnico;
	}

	public Tecnico seleccionarTecnico(Integer id) {
		EntityManager em = emf.createEntityManager();
		TecnicoDAO dao = new TecnicoDAO(em);
		Tecnico tecnico = null;
		try {
			em.getTransaction().begin();
			tecnico = dao.obtenerTecnico(id);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return tecnico;
	}

	public Tecnico seleccionarTecnicoPorNombre(String nombre) {
		EntityManager em = emf.createEntityManager();
		TecnicoDAO dao = new TecnicoDAO(em);
		Tecnico tecnico = null;
		try {
			em.getTransaction().begin();
			tecnico = dao.obtenerTecnicoPorNombre(nombre);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return tecnico;
	}
	
	// FIXME
	public void asignarProcedimiento(Integer idTecnico,
			String codigoProcedimiento) {
		EntityManager em = emf.createEntityManager();

		try {
			em.getTransaction().begin();
			Tecnico t = em.find(TecnicoImpl.class, idTecnico);
			Procedimiento p = em.find(ProcedimientoImpl.class,
					codigoProcedimiento);
			t.addProcedimiento(p);
			em.getTransaction().commit();
		} catch (NoResultException nre) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
	}

	// FIXME
	public void reclamarIncidencia(Integer idTecnico, Integer idIncidencia) {
		EntityManager em = emf.createEntityManager();
		
		try {
			em.getTransaction().begin();
			Tecnico t = em.find(TecnicoImpl.class, idTecnico);
			Incidencia inc = em.find(IncidenciaImpl.class, idIncidencia);
			t.addIncidencia(inc);
			inc.setTecnico(t);
			em.getTransaction().commit();
		} catch (NoResultException nre) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
	}

}
