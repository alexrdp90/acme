package isw2.service.incidencia;

import isw2.domain.incidencia.Incidencia;
import isw2.domain.incidencia.IncidenciaImpl;
import isw2.infrastructure.IncidenciaRepository;
import isw2.infrastructure.ProcedimientoDAO;
import isw2.infrastructure.ProductoRepository;
import isw2.infrastructure.RespuestaRepository;

import java.util.Calendar;
import java.util.List;

import isw2.domain.procedimiento.Procedimiento;
import isw2.domain.producto.Producto;
import isw2.domain.respuesta.Respuesta;
import isw2.domain.respuesta.RespuestaImpl;
import isw2.domain.tecnico.TecnicoSystem;

public class IncidenciaServiceImpl implements IncidenciaService {

	private IncidenciaRepository repository;

	public IncidenciaServiceImpl() {
		repository = new IncidenciaRepository();
	}

	public Incidencia registrarIncidencia(String cliente, String email,
			String descripcion, Calendar fechaCompra, String codigoProducto,
			String codigoProcedimiento, List<Incidencia> incidencias) {
		
		ProductoRepository repoProd = new ProductoRepository();
		ProcedimientoDAO repoProc = new ProcedimientoDAO(); 
		Producto producto = repoProd.find(codigoProducto);
		Procedimiento procedimiento = repoProc.find(codigoProcedimiento);

		Incidencia incidencia = new IncidenciaImpl(cliente, email, descripcion,
				fechaCompra, producto, procedimiento, incidencias);
		if (codigoProcedimiento == null) {
			incidencia.setTecnico(TecnicoSystem.getInstance());
			incidencia.resolver();
		}
		repository.store(incidencia);
		return incidencia;
	}
	
	public Incidencia seleccionarIncidencia(Integer id) {
		return repository.find(id);
	}

	public List<Incidencia> listarIncidencias() {
		return repository.findAll();
	}

	public Respuesta resolver(Integer idIncidencia, String descripcion) {
		RespuestaRepository repo = new RespuestaRepository();
		Incidencia inc = seleccionarIncidencia(idIncidencia);
		inc.resolver();
		Respuesta respuesta = new RespuestaImpl(inc, descripcion, 0);
		repository.update(inc);
		repo.store(respuesta);
		return respuesta;
	}

	public Respuesta addRespuesta(Integer idIncidencia, String descripcion,
			Integer valoracion) {
		RespuestaRepository repositoryRespuesta = new RespuestaRepository();
		Incidencia inc = seleccionarIncidencia(idIncidencia);
		Respuesta respuesta = new RespuestaImpl(inc, descripcion,
				valoracion);
		repositoryRespuesta.store(respuesta);
		return respuesta;
	}

	public void valorarRespuesta(Integer idRespuesta, Integer valoracion) {
		RespuestaRepository repositoryRespuesta = new RespuestaRepository();
		repositoryRespuesta.setValoracion(idRespuesta, valoracion);
	}

}
