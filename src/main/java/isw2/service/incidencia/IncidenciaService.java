package isw2.service.incidencia;

import isw2.domain.incidencia.Incidencia;
import isw2.domain.respuesta.Respuesta;

import java.util.Calendar;
import java.util.List;

public interface IncidenciaService {
	
	public Incidencia registrarIncidencia(String cliente, String email,
			String descripcion, Calendar fechaCompra, String codigoProducto,
			String codigoProcedimiento, List<Incidencia> incidencias);

	public Incidencia seleccionarIncidencia(Integer id);
	
	public List<Incidencia> listarIncidencias();

	public Respuesta resolver(Integer idIncidencia, String descripcion);
	
	public void valorarRespuesta(Integer idRespuesta, Integer valoracion);

}
