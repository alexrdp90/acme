package isw2.domain.producto;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.*;

import isw2.domain.procedimiento.Procedimiento;
import isw2.domain.procedimiento.ProcedimientoImpl;

@Entity
@Table(name = "productos")
public class ProductoImpl implements Producto, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String codigo;
	private String nombre;
	private String descripcion;
	private Boolean visible;

	@ManyToMany(targetEntity = ProcedimientoImpl.class)
	private List<Procedimiento> procedimientos;

	public ProductoImpl() {
	}

	public ProductoImpl(String codigo, String nombre, String descripcion) {
		setCodigo(codigo);
		setNombre(nombre);
		setDescripcion(descripcion);
		setVisiblilidad(true);
		this.procedimientos = new LinkedList<Procedimiento>();
	}

	public String getCodigo() {
		return codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public Boolean esVisible() {
		return visible;
	}

	public List<Procedimiento> getProcedimientos() {
		return procedimientos;
	}

	public void setCodigo(String codigo) {
		if (codigo == null) {
			throw new IllegalArgumentException("Codigo no valido");
		}
		this.codigo = codigo;
	}

	public void setNombre(String nombre) {
		if (nombre == null || nombre.equals("")) {
			throw new IllegalArgumentException("Nombre no valido");
		}
		this.nombre = nombre;
	}

	public void setDescripcion(String desc) {
		if (desc == null || desc.equals("")) {
			throw new IllegalArgumentException("Descipcion no valida");
		}
		this.descripcion = desc;
	}

	public void setVisiblilidad(Boolean v) {
		if (v == null) {
			throw new IllegalArgumentException("Visibilidad no valida");
		}
		this.visible = v;
	}

	public void addProcedimiento(Procedimiento proc) {
		if (proc == null) {
			throw new IllegalArgumentException("Procedimiento vacio");
		}
		procedimientos.add(proc);
	}

	public void removeProcedimiento(Procedimiento proc) {
		if (proc == null) {
			throw new IllegalArgumentException("Procedimiento no valido");
		}
		procedimientos.remove(proc);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ProductoImpl))
			return false;
		ProductoImpl other = (ProductoImpl) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}

}
