package isw2.domain.producto;

import isw2.domain.procedimiento.Procedimiento;

import java.util.List;

import tdg.contract.semanticAnnotations.*;

@Init({ "getNombre() != null && !getNombre().equals(\"\")", // Obligatorio
		"getCodigo() != null && !getCodigo().equals(\"\")", // Obligatorio
		"getDescripcion() != null",
		"getProcedimientos().size() == 1" }) // Procedimiento "Otro..."
@Inv({ "getProcedimientos().size() > 1" })
public interface Producto {

	@Query
	String getCodigo();

	@Query
	String getNombre();

	@Query
	String getDescripcion();

	@Query
	Boolean esVisible();

	@Query
	List<Procedimiento> getProcedimientos();

	@Pre({ "codigo != null" })
	@Pos({ "getCodigo().equals(codigo)",
			"getNombre().equals(getNombre()@pre)",
			"getDescripcion().equals(getDescripcion()@pre)",
			"esVisible() == esVisible()@pre",
			"getProcedimientos().equals(getProcedimientos()@pre)"})
	void setCodigo(String codigo);

	@Pre({ "nombre != null" })
	@Pos({ "getCodigo().equals(getCodigo()@pre)",
			"getNombre().equals(nombre)",
			"getDescripcion().equals(getDescripcion()@pre)",
			"esVisible() == esVisible()@pre",
			"getProcedimientos().equals(getProcedimientos()@pre)" })
	void setNombre(String nombre);

	@Pre({ "desc != null && !desc.equals(\"\")" })
	@Pos({ "getCodigo().equals(getCodigo()@pre)",
			"getNombre().equals(getNombre()@pre)",
			"getDescripcion().equals(desc)",
			"esVisible() == esVisible()@pre",
			"getProcedimientos().equals(getProcedimientos()@pre)" })
	void setDescripcion(String desc);

	@Pre({ "v != null" })
	@Pos({ "getNombre().equals(getNombre()@pre)",
			"getCodigo().equals(getCodigo()@pre)",
			"getDescripcion().equals(getDescripcion()@pre)",
			"esVisible() == v ",
			"getProcedimientos().equals(getProcedimientos()@pre)" })
	void setVisiblilidad(Boolean v);
	
	@Pre({ "proc != null", "!getProcedimientos().contains(proc)" })
	@Pos({ "getProcedimientos().contains(proc)",
			"forall elt: (getProcedimientos()@pre) ~ (getProcedimientos().contains(elt))",
			"getNombre() == getNombre()@pre",
			"getCodigo() == getCodigo()@pre",
			"getDescripcion() == getDescripcion()@pre",
			"esVisible() == esVisible()@pre" })
	void addProcedimiento(Procedimiento proc);
	
	@Pre({ "proc != null" })
	@Pos({ "!getProcedimientos().contains(proc)",
			"forall elt: (getProcedimientos()@pre) ~ (getProcedimientos().contains(elt) || elt == proc)",
			"getNombre() == getNombre()@pre",
			"getCodigo() == getCodigo()@pre",
			"getDescripcion() == getDescripcion()@pre",
			"esVisible() == esVisible()@pre" })
	void removeProcedimiento(Procedimiento proc);

}
