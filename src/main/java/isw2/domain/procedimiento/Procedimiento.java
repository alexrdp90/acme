package isw2.domain.procedimiento;

import tdg.contract.semanticAnnotations.*;

@Init({ "getNombre() != null && !getNombre().equals(\"\")",
		"getCodigo() != null && !getCodigo().equals(\"\")",
		"getDescripcion() != null" })
@Inv({})
public interface Procedimiento {

	@Query
	String getCodigo();

	@Query
	String getNombre();

	@Query
	String getDescripcion();

	@Pre({ "codigo != null && !codigo.equals(\"\")" })
	@Pos({ "getCodigo().equals(codigo)", "getNombre().equals(getNombre()@pre)",
			"getDescripcion().equals(getDescripcion()@pre)" })
	void setCodigo(String codigo);

	@Pre({ "nombre != null && !nombre.equals(\"\")" })
	@Pos({ "getNombre().equals(nombre)", "getCodigo().equals(getCodigo()@pre)",
			"getDescripcion().equals(getDescripcion()@pre)" })
	void setNombre(String nombre);

	@Pre({ "descripcion != null && !descripcion.equals(\"\")" })
	@Pos({ "getDescripcion().equals(descripcion)",
			"getNombre().equals(getNombre()@pre)",
			"getCodigo().equals(getCodigo()@pre)" })
	void setDescripcion(String descripcion);

}
