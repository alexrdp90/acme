package isw2.domain.procedimiento;

import isw2.service.procedimiento.ProcedimientoServiceImpl;

public class NoProc {
	private static Procedimiento noProc;
	
	public static Procedimiento getInstance() {
		if(noProc == null){
			noProc = new ProcedimientoServiceImpl().seleccionarProcedimiento("NO-PROC");
		}
		return noProc;
	}
}
