package isw2.domain.procedimiento;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "procedimientos")
public class ProcedimientoImpl implements Procedimiento, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String codigo;
	private String nombre;
	private String descripcion;

	public ProcedimientoImpl(String codigo, String nombre, String descripcion) {
		setCodigo(codigo);
		setNombre(nombre);
		setDescripcion(descripcion);
	}

	public ProcedimientoImpl() {

	}

	public String getCodigo() {
		return codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setCodigo(String codigo) {
		if (codigo == null || codigo.equals("")) {
			throw new IllegalArgumentException("Codigo no valido");
		}
		this.codigo = codigo;
	}

	public void setNombre(String nombre) {
		if (nombre == null || nombre.equals("")) {
			throw new IllegalArgumentException("Nombre no valido");
		}
		this.nombre = nombre;
	}

	public void setDescripcion(String desc) {
		if (desc == null || desc.equals("")) {
			throw new IllegalArgumentException("Descipcion no valida");
		}
		this.descripcion = desc;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ProcedimientoImpl))
			return false;
		ProcedimientoImpl other = (ProcedimientoImpl) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}

}
