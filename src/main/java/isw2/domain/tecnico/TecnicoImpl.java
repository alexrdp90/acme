package isw2.domain.tecnico;

import isw2.domain.incidencia.Incidencia;
import isw2.domain.incidencia.IncidenciaImpl;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.*;

import isw2.domain.procedimiento.Procedimiento;
import isw2.domain.procedimiento.ProcedimientoImpl;

@Entity
@Table(name = "tecnicos", uniqueConstraints = @UniqueConstraint(columnNames = "nombre"))
public class TecnicoImpl implements Tecnico, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String nombre;
	private String password;
	private String telefono;
	private Boolean visible = false;

	@ManyToMany(targetEntity = ProcedimientoImpl.class)
	@JoinTable(name = "tecnicos_procedimientos")
	private List<Procedimiento> procedimientos;

	@OneToMany(targetEntity = IncidenciaImpl.class, mappedBy = "tecnico")
	private List<Incidencia> incidencias;

	public TecnicoImpl() {
	}

	public TecnicoImpl(String nombre) {
		setNombre(nombre);
		this.procedimientos = new LinkedList<Procedimiento>();
		this.incidencias = new LinkedList<Incidencia>();
	}

	public TecnicoImpl(String nombre, String password, String telefono) {
		this(nombre);
		setPassword(password);
		setTelefono(telefono);
		setVisiblilidad(true);
	}

	public String getNombre() {
		return nombre;
	}

	public String getPassword() {
		return password;
	}

	public String getTelefono() {
		return telefono;
	}

	public Boolean esVisible() {
		return visible;
	}

	public List<Procedimiento> getProcedimientos() {
		return procedimientos;
	}

	public List<Incidencia> getIncidencias() {
		return incidencias;
	}

	public void setNombre(String nombre) {
		if (nombre == null || nombre.equals("")) {
			throw new IllegalArgumentException("Nombre no valido");
		}
		this.nombre = nombre;
	}

	public void setPassword(String password) {
		if (password == null || password.equals("")) {
			throw new IllegalArgumentException("Password invalido");
		}
		this.password = password;
	}

	public void setTelefono(String telefono) {
		if (telefono == null || !telefono.matches("(\\d{2}\\+)?\\d{9}")) {
			throw new IllegalArgumentException("Formato de telefono no valido");
		}
		this.telefono = telefono;
	}

	public void setVisiblilidad(Boolean v) {
		if (v == null) {
			throw new IllegalArgumentException("Visibilidad incorrecta");
		}
		this.visible = v;
	}

	public void addProcedimiento(Procedimiento proc) {
		if (proc == null) {
			throw new IllegalArgumentException("Procedimiento vacio");
		}
		procedimientos.add(proc);
	}

	public void removeProcedimiento(Procedimiento proc) {
		if (proc == null) {
			throw new IllegalArgumentException("Procedimiento vacio");
		}
		procedimientos.remove(proc);
	}

	public void addIncidencia(Incidencia inc) {
		if (inc == null) {
			throw new IllegalArgumentException("Incidencia vacio");
		}
		incidencias.add(inc);
	}

	public void removeIncidencia(Incidencia inc) {
		if (inc == null) {
			throw new IllegalArgumentException("Incidencia vacio");
		}
		incidencias.remove(inc);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof TecnicoImpl))
			return false;
		TecnicoImpl other = (TecnicoImpl) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Integer getId() {
		return id;
	}

}
