package isw2.domain.tecnico;

import isw2.service.tecnico.TecnicoServiceImpl;

public class TecnicoSystem {
	private static Tecnico system;

	public static Tecnico getInstance() {
		if (system == null) {
			system = new TecnicoServiceImpl().seleccionarTecnico(1);
		}
		return system;
	}
}
