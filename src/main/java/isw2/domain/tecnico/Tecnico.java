package isw2.domain.tecnico;

import isw2.domain.incidencia.Incidencia;
import isw2.domain.procedimiento.Procedimiento;

import java.util.List;

import tdg.contract.semanticAnnotations.*;

@Init({
		"getNombre() != null && getNombre().equals(\"\")", // Obligatorio
		"getPassword() != null && getPassword().equals(\"\")", // Obligatorio
		"getTelefono() != null", "esVisible()",
		"getProcedimientos() != null", "getIncidencias() != null" })
@Inv({ "getProcedimientos().size() <= 10" })
public interface Tecnico {
	
	@Query
	Integer getId();
	
	@Query
	String getNombre();

	@Query
	String getPassword();

	@Query
	String getTelefono();

	@Query
	Boolean esVisible();

	@Query
	List<Procedimiento> getProcedimientos();

	@Query
	List<Incidencia> getIncidencias();

	@Pre({ "nombre != null && !nombre.equals(\"\")" })
	@Pos({ "getNombre().equals(nombre)", "getPassword().equals(getPassword()@pre)",
			"getTelefono().equals(getTelefono()@pre)",
			"esVisible() == esVisible()@pre",
			"getProcedimientos() == getProcedimientos()@pre",
			"getIncidencias() == getIncidencias()@pre" })
	void setNombre(String nombre);

	@Pre({ "password != null && !password.equals(\"\")" })
	@Pos({ "getPassword().equals(password)", "getNombre().equals(getNombre()@pre)",
			"getTelefono().equals(getTelefono()@pre)",
			"esVisible() == esVisible()@pre",
			"getProcedimientos() == getProcedimientos()@pre",
			"getIncidencias() == getIncidencias()@pre" })
	void setPassword(String password);

	@Pre({ "telefono != null && telefono.matches(\"(\\d{2}\\+)?\\d{9}\")" })
	@Pos({ "getTelefono().equals(telefono)",
			"getPassword().equals(getPassword()@pre)",
			"getNombre().equals(getNombre()@pre)", "esVisible() == esVisible()@pre",
			"getProcedimientos() == getProcedimientos()@pre",
			"getIncidencias() == getIncidencias()@pre" })
	void setTelefono(String telefono);

	@Pre({ "v != null" })
	@Pos({ "esVisible() == v", "getNombre() == getNombre()@pre",
			"getPassword() == getPassword()@pre",
			"getTelefono() == getTelefono()@pre",
			"getProcedimientos() == getProcedimientos()@pre",
			"getIncidencias() == getIncidencias()@pre" })
	void setVisiblilidad(Boolean v);

	@Pre({ "proc != null", "getProcedimientos().size() < 10",
			"!getProcedimientos().contains(proc)" })
	@Pos({
			"getProcedimientos().contains(proc)",
			"forall p: getProcedimientos()@pre ~ getProcedimientos().contains(p)",
			"getNombre() == getNombre()@pre",
			"getPassword() == getPassword()@pre",
			"getTelefono() == getTelefono()@pre",
			"esVisible() == esVisible()@pre",
			"getIncidencias() == getIncidencias()@pre" })
	void addProcedimiento(Procedimiento proc);

	@Pre({ "proc != null" })
	@Pos({
			"getProcedimientos().contains(proc)@pre ==> !getProcedimientos().contains(proc)",
			"forall p: getProcedimientos()@pre ~ getProcedimientos().contains(p)",
			"getNombre().equals(getNombre()@pre)",
			"getPassword().equals(getPassword()@pre)",
			"getTelefono().equals(getTelefono()@pre)",
			"esVisible() == esVisible()@pre",
			"getIncidencias().equals(getIncidencias()@pre)" })
	void removeProcedimiento(Procedimiento proc);
	
	@Pre({ "inc != null", "getIncidencias().size() < 10",
	"!getIncidencias().contains(inc)" })
	@Pos({
		"getIncidencias().contains(inc)",
		"forall p: getIncidencias()@pre ~ getIncidencias().contains(p)",
		"getNombre() == getNombre()@pre",
		"getPassword() == getPassword()@pre",
		"getTelefono() == getTelefono()@pre",
		"esVisible() == esVisible()@pre",
		"getProcedimientos() == getProcedimientos()@pre" })
	void addIncidencia(Incidencia inc);

	@Pre({ "inc != null" })
	@Pos({
		"getIncidencias().contains(inc)@pre ==> !getIncidencias().contains(inc)",
		"forall p: getIncidencias()@pre ~ getIncidencias().contains(p)",
		"getNombre().equals(getNombre()@pre)",
		"getPassword().equals(getPassword()@pre)",
		"getTelefono().equals(getTelefono()@pre)",
		"esVisible() == esVisible()@pre",
		"getProcedimientos().equals(getProcedimientos()@pre)" })
	void removeIncidencia(Incidencia inc);

}
