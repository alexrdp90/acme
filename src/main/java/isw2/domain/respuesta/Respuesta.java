package isw2.domain.respuesta;

import isw2.domain.incidencia.Incidencia;

import java.util.Calendar;

import tdg.contract.semanticAnnotations.*;

@Init({ "getIncidencia() != null", "getFecha() != null",
		"getDescripcion() != null && !getDescripcion().equals(\"\")",
		"getValoracion() == null" })
@Inv({})
public interface Respuesta {

	@Query
	Incidencia getIncidencia();

	@Query
	String getDescripcion();

	@Query
	Integer getValoracion();

	@Query
	Calendar getFecha();

	@Pre({ "inc != null" })
	@Pos({ "getIncidencia() == inc", "getDescripcion() == getDescripcion()@pre",
			"getValoracion() == getValoracion()@pre",
			"getFecha() == getFecha()@pre" })
	void setIncidencia(Incidencia inc);

	@Pre({ "desc != null && !desc.equals(\"\")" })
	@Pos({ "getDescripcion() == desc",
			"getIncidencia() == getIncidencia()@pre",
			"getValoracion() == getValoracion()@pre",
			"getFecha() == getFecha()@pre" })
	void setDescripcion(String desc);

	@Pre({ "val != null"})
	@Pos({ "getValoracion() == val", "getIncidencia() == getIncidencia()@pre",
			"getDescripcion() == getDescripcion()@pre",
			"getFecha() == getFecha()@pre" })
	void setValoracion(Integer val);

}
