package isw2.domain.respuesta;

import isw2.domain.incidencia.Incidencia;
import isw2.domain.incidencia.IncidenciaImpl;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "respuestas")
public class RespuestaImpl implements Respuesta, Serializable {

	private static final long serialVersionUID = 1L;
	private static int VALORACION_MAXIMA = 5;
	private static int VALORACION_MINIMA = -5;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String descripcion;
	private Integer valoracion;

	@Temporal(TemporalType.DATE)
	private Calendar fecha;
	
	@OneToOne(mappedBy = "respuesta", targetEntity = IncidenciaImpl.class, fetch = FetchType.LAZY)
	private Incidencia incidencia;

	public RespuestaImpl() {
	}

	public RespuestaImpl(Incidencia incidencia, String descripcion, Integer valoracion) {
		setIncidencia(incidencia);
		setDescripcion(descripcion);
		setValoracion(valoracion);
		this.fecha = new GregorianCalendar();
	}

	public Incidencia getIncidencia() {
		return incidencia;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public Integer getValoracion() {
		return valoracion;
	}

	public Calendar getFecha() {
		return fecha;
	}

	public void setIncidencia(Incidencia inc) {
		if (inc == null) {
			throw new IllegalArgumentException();
		}
		this.incidencia = inc;
	}

	public void setDescripcion(String desc) {
		if (desc == null) {
			throw new IllegalArgumentException();
		}
		this.descripcion = desc;
	}

	public void setValoracion(Integer val) {
		if (val == null || val > VALORACION_MAXIMA || val < VALORACION_MINIMA) {
			throw new IllegalArgumentException();
		}
		this.valoracion = val;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof RespuestaImpl))
			return false;
		RespuestaImpl other = (RespuestaImpl) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
