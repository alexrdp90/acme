package isw2.domain.incidencia;

import isw2.domain.respuesta.Respuesta;
import isw2.domain.respuesta.RespuestaImpl;
import isw2.domain.tecnico.Tecnico;
import isw2.domain.tecnico.TecnicoImpl;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.*;

import isw2.domain.procedimiento.Procedimiento;
import isw2.domain.procedimiento.ProcedimientoImpl;
import isw2.domain.producto.Producto;
import isw2.domain.producto.ProductoImpl;

@Entity
@Table(name = "incidencias")
public class IncidenciaImpl implements Incidencia, Serializable {

	private static final long serialVersionUID = 1L;
	private static final String EMAIL_PATTERN = "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String cliente;
	private String email;
	private String descripcion;
	@SuppressWarnings("unused")
	private Boolean resuelta;

	@Temporal(TemporalType.DATE)
	private Calendar fechaCompra;

	@Temporal(TemporalType.DATE)
	private Calendar fecha;

	@ManyToOne(targetEntity = ProcedimientoImpl.class)
	private Procedimiento procedimiento;

	@ManyToOne(targetEntity = TecnicoImpl.class)
	private Tecnico tecnico;

	@ManyToOne(targetEntity = ProductoImpl.class)
	private Producto producto;

	@OneToOne(targetEntity = RespuestaImpl.class)
	private Respuesta respuesta;

	@ManyToMany(targetEntity = IncidenciaImpl.class)
	private List<Incidencia> incidenciasRel;

	public IncidenciaImpl() {
	}

	public IncidenciaImpl(String cliente, String email, String descripcion,
			Calendar fechaCompra, Producto producto,
			Procedimiento procedimiento, List<Incidencia> incidencias) {
		setCliente(cliente);
		setEmail(email);
		setDescripcion(descripcion);
		setFecha(new GregorianCalendar());
		setFechaCompra(fechaCompra);
		setProducto(producto);
		setProcedimiento(procedimiento);
		this.resuelta = false;
		this.incidenciasRel = new LinkedList<Incidencia>();
		for (Incidencia inc : incidencias)
			addIncidencia(inc);
	}

	public String getCliente() {
		return cliente;
	}

	public String getEmail() {
		return email;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public Calendar getFechaCompra() {
		return fechaCompra;
	}

	public Calendar getFecha() {
		return fecha;
	}

	public Procedimiento getProcedimiento() {
		return procedimiento;
	}

	public Tecnico getTecnico() {
		return tecnico;
	}

	public Producto getProducto() {
		return producto;
	}

	public Respuesta getRespuesta() {
		return respuesta;
	}

	public List<Incidencia> getIncidencias() {
		return incidenciasRel;
	}

	public void resolver() {
		this.resuelta = true;
	}

	public void setCliente(String cliente) {
		if (cliente == null || cliente.equals("")) {
			throw new IllegalArgumentException("Nombre de cliente invalido");
		}
		this.cliente = cliente;
	}

	public void setEmail(String email) {
		if (email == null) {
			throw new IllegalArgumentException("Direccion de email no valida");
		}
		if (!email.matches(IncidenciaImpl.EMAIL_PATTERN)) {
			throw new IllegalArgumentException(
					"Formato de direccion de email no valida");
		}
		this.email = email;
	}

	public void setDescripcion(String desc) {
		if (desc == null) {
			throw new IllegalArgumentException(
					"Descripcion de incidencia invalida");
		}
		this.descripcion = desc;
	}

	public void setFechaCompra(Calendar cal) {
		if (cal == null) {
			throw new IllegalArgumentException("Fecha de compra invalida");
		}
		this.fechaCompra = cal;
	}

	public void setFecha(Calendar cal) {
		if (cal == null) {
			throw new IllegalArgumentException("Fecha invalida");
		}
		this.fecha = cal;
	}

	public void setProcedimiento(Procedimiento proc) {
		this.procedimiento = proc;
	}

	public void setProducto(Producto prod) {
		if (prod == null) {
			throw new IllegalArgumentException("Producto invalido");
		}
		this.producto = prod;
	}

	public void addIncidencia(Incidencia inc) {
		if (inc == null) {
			throw new IllegalArgumentException("Incidencia vacio");
		}
		incidenciasRel.add(inc);
	}

	public void removeIncidencia(Incidencia inc) {
		if (inc == null) {
			throw new IllegalArgumentException("Incidencia vacio");
		}
		incidenciasRel.remove(inc);
	}

	public void setTecnico(Tecnico tecnico) {
		if (estaAsignada()) {
			throw new IllegalArgumentException("Tecnico ya asignado");
		}
		if (tecnico == null) {
			throw new IllegalArgumentException("Tecnico vacio");
		}
		this.tecnico = tecnico;
	}

	public void setRespuesta(Respuesta respuesta) {
		this.respuesta = respuesta;
	}

	public Boolean estaAsignada() {
		return this.tecnico != null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof IncidenciaImpl))
			return false;
		IncidenciaImpl other = (IncidenciaImpl) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Integer getId() {
		return id;
	}
	
	public String toString() {
		return "Incidencia #" + id + " por " + email;
	}

}
