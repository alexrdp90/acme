package isw2.domain.incidencia;

import isw2.domain.respuesta.Respuesta;
import isw2.domain.tecnico.Tecnico;
import isw2.domain.procedimiento.Procedimiento;
import isw2.domain.producto.Producto;
import java.util.Calendar;
import java.util.List;
import tdg.contract.semanticAnnotations.*;

@Init({
		"getCliente() != null && getCliente() != \"\"", // Obligatorio
		"getEmail() != null && getEmail() != \"\"", // Obligatorio
		"getDescripcion() != null",
		"getTecnico() == null", // Vacio hasta que se asigne
		"getFechaCompra() != null", "getFecha() != null" })
@Inv({})
public interface Incidencia {
	
	@Query
	String getCliente();

	@Query
	String getEmail();

	@Query
	String getDescripcion();

	@Query
	Calendar getFechaCompra();

	@Query
	Calendar getFecha();

	@Query
	Procedimiento getProcedimiento();

	@Query
	Producto getProducto();

	@Query
	Tecnico getTecnico();

	@Query
	Respuesta getRespuesta();

	@Query
	List<Incidencia> getIncidencias();

	@Query
	@Pos({ "result == (getTecnico() != null)" })
	Boolean estaAsignada();
	
	@Pre({ "cliente != null && cliente != \"\"" })
	void resolver();

	@Pre({ "cliente != null && cliente != \"\"" })
	@Pos({ "getCliente() == cliente", "getEmail() == getEmail()@pre",
			"getDescripcion() == getDescripcion()@pre",
			"getProcedimiento() == getProcedimiento()@pre",
			"getTecnico() == getTecnico()@pre",
			"getFechaCompra() == getFechaCompra()@pre",
			"getFecha() == getFecha()@pre" })
	void setCliente(String cliente);

	@Pre({ "email != null && email.matches(\"[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})\")" })
	@Pos({ "getEmail() == email", "getCliente() == getCliente()@pre",
			"getDescripcion() == getDescripcion()@pre",
			"getProcedimiento() == getProcedimiento()@pre",
			"getTecnico() == getTecnico()@pre",
			"getFechaCompra() == getFechaCompra()@pre",
			"getFecha() == getFecha()@pre" })
	void setEmail(String email);

	@Pre({ "desc != null && desc != \"\"" })
	@Pos({ "getDescripcion() == desc", "getCliente() == getCliente()@pre",
			"getEmail() == getEmail()@pre",
			"getProcedimiento() == getProcedimiento()@pre",
			"getTecnico() == getTecnico()@pre",
			"getFechaCompra() == getFechaCompra()@pre",
			"getFecha() == getFecha()@pre" })
	void setDescripcion(String desc);

	@Pre({ "cal != null" })
	@Pos({ "getFechaCompra() == cal", "getTecnico() == getTecnico()@pre",
			"getCliente() == getCliente()@pre", "getEmail() == getEmail()@pre",
			"getDescripcion() == getDescripcion()@pre",
			"getProcedimiento() == getProcedimiento()@pre",
			"getFecha() == getFecha()@pre" })
	void setFechaCompra(Calendar cal);

	@Pre({ "cal != null" })
	@Pos({ "getFecha() == cal", "getTecnico() == getTecnico()@pre",
			"getCliente() == getCliente()@pre", "getEmail() == getEmail()@pre",
			"getDescripcion() == getDescripcion()@pre",
			"getProcedimiento() == getProcedimiento()@pre",
			"getFechaCompra() == getFechaCompra()@pre" })
	void setFecha(Calendar cal);

	@Pos({ "getProcedimiento() == proc", "getCliente() == getCliente()@pre",
			"getEmail() == getEmail()@pre",
			"getDescripcion() == getDescripcion()@pre",
			"getTecnico() == getTecnico()@pre",
			"getFechaCompra() == getFechaCompra()@pre",
			"getFecha() == getFecha()@pre" })
	void setProcedimiento(Procedimiento proc);

	@Pre({ "prod != null" })
	@Pos({ "getProducto() == prod", "getCliente() == getCliente()@pre",
			"getEmail() == getEmail()@pre",
			"getDescripcion() == getDescripcion()@pre",
			"getTecnico() == getTecnico()@pre",
			"getCliente() == getCliente()@pre",
			"getFechaCompra() == getFechaCompra()@pre",
			"getFecha() == getFecha()@pre" })
	void setProducto(Producto prod);

	@Pre({ "tecnico != null", "getTecnico() == null" })
	@Pos({ "getProducto() == tecnico", "getCliente() == getCliente()@pre",
			"getEmail() == getEmail()@pre",
			"getDescripcion() == getDescripcion()@pre",
			"getTecnico() == getTecnico()@pre",
			"getCliente() == getCliente()@pre",
			"getFechaCompra() == getFechaCompra()@pre",
			"getFecha() == getFecha()@pre" })
	void setTecnico(Tecnico tecnico);

	@Pre({ "respuesta != null" })
	@Pos({ "getProducto() == respuestas", "getCliente() == getCliente()@pre",
			"getEmail() == getEmail()@pre",
			"getDescripcion() == getDescripcion()@pre",
			"getTecnico() == getTecnico()@pre",
			"getCliente() == getCliente()@pre",
			"getFechaCompra() == getFechaCompra()@pre",
			"getFecha() == getFecha()@pre" })
	void setRespuesta(Respuesta respuesta);

	@Pre({ "inc != null", "getIncidencias().size() < 10",
			"!getIncidencias().contains(inc)" })
	@Pos({ "getIncidencias().contains(inc)",
			"forall p: getIncidencias()@pre ~ getIncidencias().contains(p)",
			"getCliente() == getCliente()@pre", "getEmail() == getEmail()@pre",
			"getDescripcion() == getDescripcion()@pre",
			"getTecnico() == getTecnico()@pre",
			"getCliente() == getCliente()@pre",
			"getFechaCompra() == getFechaCompra()@pre",
			"getFecha() == getFecha()@pre" })
	void addIncidencia(Incidencia inc);

	@Pre({ "inc != null" })
	@Pos({
			"getIncidencias().contains(inc)@pre ==> !getIncidencias().contains(inc)",
			"forall p: getIncidencias()@pre ~ getIncidencias().contains(p)",
			"getCliente() == getCliente()@pre", "getEmail() == getEmail()@pre",
			"getDescripcion() == getDescripcion()@pre",
			"getTecnico() == getTecnico()@pre",
			"getCliente() == getCliente()@pre",
			"getFechaCompra() == getFechaCompra()@pre",
			"getFecha() == getFecha()@pre" })
	void removeIncidencia(Incidencia inc);

	@Query
	Integer getId();

}
