package isw2.presentation.server;

import javax.servlet.http.HttpServletRequest;

import isw2.domain.tecnico.Tecnico;
import isw2.presentation.client.LoginService;
import isw2.service.tecnico.TecnicoService;
import isw2.service.tecnico.TecnicoServiceImpl;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class LoginServiceImpl extends RemoteServiceServlet implements
		LoginService {

	public Boolean login(String nombre, String pass) {
		TecnicoService service = new TecnicoServiceImpl();
		Tecnico tecnico = service.seleccionarTecnicoPorNombre(nombre);
		if (isCorrectLogin(tecnico, pass)) {
			HttpServletRequest request = this.getThreadLocalRequest();
			request.getSession().setAttribute("session.id", tecnico.getId());
			request.getSession().setAttribute("session.user", tecnico.getNombre());
			return true;
		}
		return false;
	}

	private Boolean isCorrectLogin(Tecnico tecnico, String pass) {
		return tecnico != null && tecnico.getPassword() != null
				&& tecnico.getPassword().equals(pass);
	}
}
