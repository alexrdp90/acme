package isw2.presentation.server;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import isw2.domain.incidencia.Incidencia;
import isw2.infrastructure.IncidenciaRepository;
import isw2.infrastructure.ProcedimientoDAO;
import isw2.infrastructure.ProductoRepository;
import isw2.presentation.client.ClienteService;
import isw2.presentation.shared.IncidenciaDTO;
import isw2.presentation.shared.ProcedimientoDTO;
import isw2.presentation.shared.ProductoDTO;
import isw2.service.incidencia.IncidenciaService;
import isw2.service.incidencia.IncidenciaServiceImpl;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class ClienteServiceImpl extends RemoteServiceServlet implements
		ClienteService {

	public String registrarIncidencia(String cliente, String email,
			String descripcion, Date fechaCompra, String codigoProducto,
			String codigoProcedimiento, List<Integer> idIncidencias)
			throws IllegalArgumentException {

		Calendar calFechaCompra = Calendar.getInstance();
		calFechaCompra.setTime(fechaCompra);

		IncidenciaService service = new IncidenciaServiceImpl();
		List<Incidencia> incidencias = new LinkedList<Incidencia>();
		for (Integer id : idIncidencias) {
			incidencias.add(service.seleccionarIncidencia(id));
		}
		service.registrarIncidencia(cliente, email, descripcion,
				calFechaCompra, codigoProducto, codigoProcedimiento,
				incidencias);
		return "Incidencia registrada con exito";
	}

	public String visualizarIncidencia(Integer idIncidencia) {
		IncidenciaService service = new IncidenciaServiceImpl();
		return service.seleccionarIncidencia(idIncidencia).toString();
	}

	public String addRespuesta(Integer idRespuesta, Integer valoracion)
			throws IllegalArgumentException {
		IncidenciaService service = new IncidenciaServiceImpl();
		service.valorarRespuesta(idRespuesta, valoracion);
		return "Respuesta registrada con exito";
	}

	public List<ProductoDTO> listarProductos() {
		ProductoRepository repo = new ProductoRepository();
		return repo.findAllDTOs();
	}

	public List<ProcedimientoDTO> listarProcedimientos() {
		ProcedimientoDAO repo = new ProcedimientoDAO();
		return repo.findAllDTOs();
	}

	public List<IncidenciaDTO> listarIncidencias() {
		IncidenciaRepository repo = new IncidenciaRepository();
		List<Incidencia> incidencias = repo.findAll();
		List<IncidenciaDTO> result = new LinkedList<IncidenciaDTO>();
		for (Incidencia inc : incidencias) {
			IncidenciaDTO incDTO = new IncidenciaDTO();
			incDTO.setId(inc.getId());
			incDTO.setCliente(inc.getCliente());
			incDTO.setEmail(inc.getEmail());
			incDTO.setDescripcion(inc.getDescripcion());
			incDTO.setProcCodigo(inc.getProcedimiento().getCodigo());
			result.add(incDTO);
		}
		return result;
	}

	public List<ProcedimientoDTO> listarProcedimientos(String codigoProducto) {
		ProcedimientoDAO repo = new ProcedimientoDAO();
		return repo.findAllProdDTOs(codigoProducto);
	}


}
