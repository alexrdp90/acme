package isw2.presentation.server;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import isw2.infrastructure.IncidenciaRepository;
import isw2.presentation.client.TecnicoLoggedService;
import isw2.presentation.shared.IncidenciaDTO;
import isw2.service.incidencia.IncidenciaService;
import isw2.service.incidencia.IncidenciaServiceImpl;
import isw2.service.tecnico.TecnicoService;
import isw2.service.tecnico.TecnicoServiceImpl;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class TecnicoLoggedServiceImpl extends RemoteServiceServlet implements
		TecnicoLoggedService {

	public List<IncidenciaDTO> listarIncidencias() {
		IncidenciaRepository repo = new IncidenciaRepository();
		HttpServletRequest request = this.getThreadLocalRequest();
		Integer idTecnico = (Integer) request.getSession().getAttribute(
				"session.id");
		if (idTecnico == null) {
			return new LinkedList<IncidenciaDTO>();
		}
		return repo.findAll(idTecnico);
	}

	public String reclamarIncidencia(Integer idIncidencia)
			throws IllegalArgumentException {
		HttpServletRequest request = this.getThreadLocalRequest();
		Integer id = (Integer) request.getSession().getAttribute(
				"session.id");
		if (id == null) {
			return "Necesita estar logueado para realizar esta accion";
		}
		TecnicoService service = new TecnicoServiceImpl();
		service.reclamarIncidencia(id, idIncidencia);
		return "Incidencia #" + idIncidencia.toString() + " reclamada";
	}

	public String resolverIncidencia(Integer idIncidencia, String descripcion)
			throws IllegalArgumentException {
		IncidenciaService inc = new IncidenciaServiceImpl();
		inc.resolver(idIncidencia, descripcion);
		return "Incidencia #" + idIncidencia.toString() + " resuelta";
	}

	public List<IncidenciaDTO> listarIncidenciasReclamadas() {
		IncidenciaRepository repo = new IncidenciaRepository();
		HttpServletRequest request = this.getThreadLocalRequest();
		Integer idTecnico = (Integer) request.getSession().getAttribute(
				"session.id");
		if (idTecnico == null) {
			return new LinkedList<IncidenciaDTO>();
		}
		return repo.findAllAvailables(idTecnico);
	}

	public void addRespuesta(String res, Integer idIncidencia) {
		IncidenciaService service = new IncidenciaServiceImpl();
		service.resolver(idIncidencia, res);
	}

}
