package isw2.presentation.server;

import java.util.List;
import java.util.Map;


import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import isw2.domain.producto.Producto;
import isw2.presentation.client.AdminService;
import isw2.presentation.shared.ProcedimientoDTO;
import isw2.presentation.shared.ProductoDTO;
import isw2.presentation.shared.TecnicoDTO;
import isw2.service.procedimiento.ProcedimientoService;
import isw2.service.procedimiento.ProcedimientoServiceImpl;
import isw2.service.producto.ProductoService;
import isw2.service.producto.ProductoServiceImpl;
import isw2.service.tecnico.TecnicoService;
import isw2.service.tecnico.TecnicoServiceImpl;

@SuppressWarnings("serial")
public class AdminServiceImpl extends RemoteServiceServlet implements
		AdminService {

	public List<TecnicoDTO> listarTecnicos() {
		TecnicoService service = new TecnicoServiceImpl();
		return service.listarTecnicosDTOs();
	}

	public List<ProcedimientoDTO> listarProcedimientos() {
		ProcedimientoService service = new ProcedimientoServiceImpl();
		return service.listarProcedimientosDTOs();
	}

	public List<ProductoDTO> listarProductos() {
		ProductoService service = new ProductoServiceImpl();
		return service.listarProcedimientosDTOs();
	}

	public TecnicoDTO añadirTecnico(String nombre, String pass, String telefono,
			List<String> procedimientos) {
		TecnicoService service = new TecnicoServiceImpl();
		service.añadirTecnico(nombre, pass, telefono, procedimientos);
		return new TecnicoDTO(nombre, telefono);
	}

	public ProcedimientoDTO añadirProcedimiento(String codigo, String nombre,
			String descripcion) {
		ProcedimientoService service = new ProcedimientoServiceImpl();
		service.darDeAltaProcedimiento(codigo, nombre, descripcion);
		return new ProcedimientoDTO(codigo, nombre, descripcion);
	}

	public ProductoDTO añadirProducto(String codigo, String nombre,
			String descripcion, List<String> procedimientos) {
		ProductoService service = new ProductoServiceImpl();
		Producto producto = service.darDeAltaProducto(codigo, nombre,
				descripcion);
		for (String s : procedimientos) {
			service.asignarProcedimiento(producto.getCodigo(), s);
		}
		return new ProductoDTO(codigo, nombre, descripcion);
	}

	public Map<TecnicoDTO, List<ProcedimientoDTO>> getProcedimientosTecnico(
			TecnicoDTO tecname, Map<TecnicoDTO, List<ProcedimientoDTO>> res) {
		ProcedimientoService service = new ProcedimientoServiceImpl();
		res.remove(tecname);
		res.put(tecname,
				service.seleccionarProcedimientoTecnico(tecname.getNombre()));
		return res;
	}

	public Map<ProductoDTO, List<ProcedimientoDTO>> getProcedimientosProducto(
			ProductoDTO prod, Map<ProductoDTO, List<ProcedimientoDTO>> res) {
		ProcedimientoService service = new ProcedimientoServiceImpl();
		res.remove(prod);
		res.put(prod,
				service.seleccionarProcedimientoProducto(prod.getCodigo()));

		return res;
	}

}
