package isw2.presentation.shared;

public class TecnicoDTO implements java.io.Serializable {

	private static final long serialVersionUID = 7811017727971963769L;

	private String nombre;
	private String telefono;
	private Boolean visible;
	
	public TecnicoDTO() {}
	
	public TecnicoDTO(String nombre, String telefono) {
		this.nombre = nombre;
		this.telefono = telefono;
	}
	
	public TecnicoDTO(String nombre, String telefono, Boolean visible) {
		this.nombre = nombre;
		this.telefono = telefono;
		this.visible = visible;
	}

	public String getNombre() {
		return nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public Boolean getVisible() {
		return visible;
	}

}
