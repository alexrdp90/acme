package isw2.presentation.shared;

public class ProcedimientoDTO implements java.io.Serializable {

	private static final long serialVersionUID = 3696592290311813433L;
	private String codigo;
	private String nombre;
	private String descripcion;

	public ProcedimientoDTO() {
	}

	public ProcedimientoDTO(String codigo, String nombre, String descipcion) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.descripcion = descipcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String toString() {
		return "Procedimiento " + getNombre();
	}

}
