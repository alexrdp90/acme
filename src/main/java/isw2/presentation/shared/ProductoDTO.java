package isw2.presentation.shared;

public class ProductoDTO implements java.io.Serializable {

	private static final long serialVersionUID = -6576485559533719295L;
	private String codigo;
	private String nombre;
	private String descripcion;

	public ProductoDTO() {
	}

	public ProductoDTO(String codigo, String nombre, String descipcion) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.descripcion = descipcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String toString() {
		return getNombre();
	}

}
