package isw2.presentation.shared;

public class IncidenciaDTO implements java.io.Serializable {

	private static final long serialVersionUID = -6711484899070449470L;

	private Integer id;
	private String cliente;
	private String email;
	private String descripcion;
	private String procCodigo;
	private Boolean resuelta;

	public IncidenciaDTO() {
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getProcCodigo() {
		return procCodigo;
	}

	public void setProcCodigo(String procCodigo) {
		this.procCodigo = procCodigo;
	}

	public Boolean getResuelta() {
		return resuelta;
	}

	public void setResuelta(Boolean resuelta) {
		this.resuelta = resuelta;
	}

	public String toString() {
		return "Incidencia #" + id + " por " + email;
	}

}
