package isw2.presentation.client;

import com.google.gwt.i18n.client.Messages;

public interface AcmeMessages extends Messages {
	
	@DefaultMessage("Técnico '{0}' dado de alta con éxito")
	String altaTecnico(String nombre);

	@DefaultMessage("Procedimiento #{0}: '{1}' registrado con éxito")
	String altaProcedimiento(String codigo, String nombre);
	
	@DefaultMessage("Producto #{0}: '{1}' registrado con éxito")
	String altaProducto(String codigo, String nombre);

}
