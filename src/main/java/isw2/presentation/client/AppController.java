package isw2.presentation.client;

import isw2.presentation.client.event.SelectionEvent;
import isw2.presentation.client.event.SelectionEventHandler;
import isw2.presentation.client.presenter.*;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.History;

public class AppController implements Presenter, ValueChangeHandler<String> {

	private final HandlerManager eventBus;
	private HasWidgets container;

	public AppController(HandlerManager eventBus) {
		this.eventBus = eventBus;
		bind();
	}

	private void bind() {
		History.addValueChangeHandler(this);

		eventBus.addHandler(SelectionEvent.TYPE, new SelectionEventHandler() {
			public void select(SelectionEvent event) {
				doSelection(event.getSelection());
			}

		});
	}

	private void doSelection(String selection) {
		History.newItem(selection);
	}

	public void go(final HasWidgets container) {
		this.container = container;

		if ("".equals(History.getToken())) {
			History.newItem("selection");
		} else {
			History.fireCurrentHistoryState();
		}
	}

	public void onValueChange(ValueChangeEvent<String> event) {
		String token = event.getValue();

		if (token != null) {
			Presenter presenter = null;

			if (token.equals("selection")) {
				presenter = new SelectionPresenter(eventBus);
			}else if (token.equals("inicio")) {
				presenter = new RegisterPresenter(eventBus);
			} else if (token.equals("admin")) {
				presenter = new AdminPresenter(eventBus);
			} else if (token.equals("login")) {
				presenter = new LoginPresenter(eventBus);
			} else if (token.equals("tecnico")) {
				presenter = new TecnicoPresenter(eventBus);
			} else if (token.equals("cliente")) {
				presenter = new ClientePresenter(eventBus);
			}
			if (presenter != null) {
				presenter.go(container);
			}
		}
	}

}
