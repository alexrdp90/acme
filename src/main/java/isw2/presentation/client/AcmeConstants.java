package isw2.presentation.client;

import com.google.gwt.i18n.client.Constants;

public interface AcmeConstants extends Constants {
	
	@DefaultStringValue("Añadir")
	String añadir();

	@DefaultStringValue("Cerrar")
	String cerrar();
	
	@DefaultStringValue("Cliente")
	String cliente();
	
	@DefaultStringValue("Código")
	String codigo();

	@DefaultStringValue("Consola de administración")
	String consolaAdministracion();
	
	@DefaultStringValue("Consola de cliente")
	String consolaCliente();
	
	@DefaultStringValue("Consola de técnico")
	String consolaTecnico();
	
	@DefaultStringValue("Descripción")
	String descripcion();
	
	@DefaultStringValue("Email")
	String email();
	
	@DefaultStringValue("Enviar")
	String enviar();

	@DefaultStringValue("Fecha de compra")
	String fechaCompra();
	
	@DefaultStringValue("Incidencia")
	String incidencia();

	@DefaultStringValue("Incidencias relacionadas")
	String incidenciasRelacionadas();
	
	@DefaultStringValue("Nombre")
	String nombre();
	
	@DefaultStringValue("Nombre de usuario")
	String nombreUsuario();
	
	@DefaultStringValue("Contraseña")
	String password();
	
	@DefaultStringValue("Procedimiento")
	String procedimiento();

	@DefaultStringValue("Procedimientos")
	String procedimientos();
	
	@DefaultStringValue("Producto")
	String producto();
	
	@DefaultStringValue("Productos")
	String productos();
	
	@DefaultStringValue("Reclamar")
	String reclamar();
	
	@DefaultStringValue("Registrar")
	String registrar();

	@DefaultStringValue("Resolver")
	String resolver();

	@DefaultStringValue("Respuesta")
	String respuesta();

	@DefaultStringValue("Resultado")
	String resultado();
	
	@DefaultStringValue("Por favor, selecione su rol")
	String seleccionarRol();

	@DefaultStringValue("Técnico")
	String tecnico();

	@DefaultStringValue("Técnicos")
	String tecnicos();

	@DefaultStringValue("Teléfono")
	String telefono();
	
}
