package isw2.presentation.client;

import isw2.presentation.shared.ProductoDTO;
import isw2.presentation.shared.TecnicoDTO;
import isw2.presentation.shared.ProcedimientoDTO;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("admin")
public interface AdminService extends RemoteService {
	List<TecnicoDTO> listarTecnicos();
	List<ProcedimientoDTO> listarProcedimientos();
	List<ProductoDTO> listarProductos();
	TecnicoDTO añadirTecnico(String nombre, String pass, String telefono,
			List<String> procedimientos);
	ProcedimientoDTO añadirProcedimiento(String codigo, String nombre,
			String descripcion);
	ProductoDTO añadirProducto(String codigo, String nombre,
			String descripcion, List<String> procedimientos);
}

