package isw2.presentation.client;

import isw2.presentation.shared.IncidenciaDTO;
import isw2.presentation.shared.ProcedimientoDTO;
import isw2.presentation.shared.ProductoDTO;

import java.util.Date;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("cliente")
public interface ClienteService extends RemoteService {
	
	String registrarIncidencia(String cliente, String email,
			String descripcion, Date fechaCompra, String codigoProducto,
			String codigoProcedimiento, List<Integer> idIncidencias)
			throws IllegalArgumentException;
	
	String visualizarIncidencia(Integer idIncidencia);
	
	List<ProductoDTO> listarProductos();
	
	List<IncidenciaDTO> listarIncidencias();
	
	List<ProcedimientoDTO> listarProcedimientos();
	
	List<ProcedimientoDTO> listarProcedimientos(String codigoProducto);
}
