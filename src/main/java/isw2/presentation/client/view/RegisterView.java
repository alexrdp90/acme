package isw2.presentation.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;

import isw2.presentation.client.AcmeConstants;
import isw2.presentation.client.presenter.RegisterPresenter;

public class RegisterView extends Composite implements RegisterPresenter.Display {

	private AcmeConstants constants = GWT.create(AcmeConstants.class);
	
	private final VerticalPanel mainPanel = new VerticalPanel();
	private final TextBox nombreTecnico = new TextBox();
	private final PasswordTextBox passTecnico = new PasswordTextBox();
	private final Button sendButton = new Button(constants.enviar());
	
	private final DialogBox dialogBox = new DialogBox();
	private final VerticalPanel dialogVPanel = new VerticalPanel();
	private final Label dialogText = new Label();
	private final Button closeButton = new Button(constants.cerrar());
	
	public RegisterView(){
		mainPanel.add(nombreTecnico);
		mainPanel.add(passTecnico);
		mainPanel.add(sendButton);

		closeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				dialogBox.hide();
				sendButton.setEnabled(true);
			}
		});
		
		dialogVPanel.add(dialogText);
		dialogVPanel.add(closeButton);
		dialogBox.setText(constants.resultado());
		dialogBox.setAnimationEnabled(true);
		dialogBox.setWidget(dialogVPanel);
		
		RootPanel.get("content").add(mainPanel);
	}
	
	public String getNombre() {
		return nombreTecnico.getText();
	}

	public String getPassword() {
		return passTecnico.getText();
	}

	public HasClickHandlers getSendButton() {
		return sendButton;
	}

	public void show(String message) {
		sendButton.setEnabled(false);
		dialogText.setText(message);
		dialogBox.center();
	}

}
