package isw2.presentation.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.event.dom.client.HasClickHandlers;

import isw2.presentation.client.AcmeConstants;
import isw2.presentation.client.presenter.SelectionPresenter;

public class SelectionView extends Composite implements
		SelectionPresenter.Display {

	private AcmeConstants constants = GWT.create(AcmeConstants.class);
	private final VerticalPanel verticalPanel = new VerticalPanel();
	private final Label labelSeleccionarRol = new Label(constants.seleccionarRol());
	private final Button consolaAdministracion = new Button(
			constants.consolaAdministracion());
	private final Button consolaTecnico = new Button(
			constants.consolaTecnico());
	private final Button consolaCliente = new Button(constants.consolaCliente());

	public SelectionView() {
		verticalPanel.add(labelSeleccionarRol);
		verticalPanel.add(consolaAdministracion);
		verticalPanel.add(consolaTecnico);
		verticalPanel.add(consolaCliente);
		initWidget(verticalPanel);
	}

	public HasClickHandlers getAdminButton() {
		return consolaAdministracion;
	}

	public HasClickHandlers getTecnicoButton() {
		return consolaTecnico;
	}

	public HasClickHandlers getClienteButton() {
		return consolaCliente;
	}
}
