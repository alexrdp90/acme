package isw2.presentation.client.view;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.HasClickHandlers;

import isw2.presentation.client.AcmeConstants;
import isw2.presentation.client.presenter.AdminPresenter;
import isw2.presentation.shared.ProcedimientoDTO;
import isw2.presentation.shared.ProductoDTO;
import isw2.presentation.shared.TecnicoDTO;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;

public class AdminView extends Composite implements AdminPresenter.Display {

	private AcmeConstants constants = GWT.create(AcmeConstants.class);
	
	private TabLayoutPanel tabLayoutPanel = new TabLayoutPanel(1.5, Unit.EM);
	private DialogBox techFrom = new DialogBox();
	private DialogBox procFrom = new DialogBox();
	private DialogBox prodFrom = new DialogBox();
	private VerticalPanel vPanelTecnicos = new VerticalPanel();
	private VerticalPanel vPanelProcedimientos = new VerticalPanel();

	private FlexTable tecnicosTable = new FlexTable();
	private FlexTable procedimientosTable = new FlexTable();
	private Button showTech = new Button("Añadir tecnico");
	private Label tecName;
	private Label techPass;
	private final VerticalPanel verticalPanel = new VerticalPanel();
	private Label tecTelf;
	private final TextBox nameTextBox = new TextBox();
	private final TextBox telftextBox_2 = new TextBox();
	private final Button Añadir = new Button("Añadir");
	private final HorizontalPanel tecHorizontalPanel = new HorizontalPanel();
	private int teci = 1;
	private int proceci = 1;
	private int produci = 1;
	private final Button procFormButton = new Button("Añadir Procedimiento");
	private final HorizontalPanel procFormHorizontal = new HorizontalPanel();
	private final VerticalPanel verticalPanel_1 = new VerticalPanel();
	private final Label label = new Label("Nombre");
	private final TextBox procTextBox = new TextBox();
	private final Label lblCodigo = new Label("Codigo");
	private final TextBox procCodigoBox = new TextBox();
	private final Button procButton = new Button("Añadir");
	private final PasswordTextBox passwordTextBox = new PasswordTextBox();
	private final Label procDescLabel = new Label("Descripcion");
	private final TextArea procDescTextArea = new TextArea();
	private final HorizontalPanel horizontalPanel = new HorizontalPanel();
	private final VerticalPanel procVerticalPanel_2 = new VerticalPanel();
	private final HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
	private final VerticalPanel verticalPanel_2 = new VerticalPanel();
	private final HorizontalPanel horizontalPanel_2 = new HorizontalPanel();
	private final Button techClose = new Button("New button");
	private final HorizontalPanel horizontalPanel_3 = new HorizontalPanel();
	private final Button procCerrar = new Button("Cerrar");
	private final VerticalPanel vPanelProductos = new VerticalPanel();
	private final HorizontalPanel horizontalPanel_4 = new HorizontalPanel();
	private final VerticalPanel verticalPanel_3 = new VerticalPanel();
	private final HorizontalPanel prodFormHorizontal = new HorizontalPanel();
	private final FlexTable productosTable = new FlexTable();
	private final Button addProductButton = new Button("New button");
	private final VerticalPanel verticalPanel_4 = new VerticalPanel();
	private final Label prodNombreLabel;
	private final TextBox prodNombreTextBox = new TextBox();
	private final Label prodCodigoLabel;
	private final TextBox prodCodigoTextBox = new TextBox();
	private final Label prodDescripcionLabel;
	private final TextArea prodTextArea = new TextArea();
	private final HorizontalPanel horizontalPanel_5 = new HorizontalPanel();
	private Button prodAddButton;
	private Button prodCloseButton;
	private final VerticalPanel ProcTecCheckBoxPanel = new VerticalPanel();
	private Label tecProcLabel;
	private final VerticalPanel productoProcedimientoPanel = new VerticalPanel();
	private final Label productoProcedimientoLabel;

	public AdminView() {
		
		prodAddButton = new Button(constants.añadir());
		prodCloseButton = new Button(constants.cerrar());
		tecProcLabel = new Label(constants.procedimientos());
		
		tecTelf = new Label(constants.telefono());
		
		tecName = new Label(constants.nombreUsuario());
		techPass = new Label(constants.password());
		prodNombreLabel = new Label(constants.nombre());
		prodCodigoLabel = new Label(constants.codigo());
		prodDescripcionLabel = new Label(constants.descripcion());
		productoProcedimientoLabel = new Label(constants.procedimientos());

		tabLayoutPanel.setAnimationVertical(true);

		tabLayoutPanel.add(vPanelTecnicos, constants.tecnicos(), false);
		vPanelTecnicos.setSize("100%", "100%");

		vPanelTecnicos.add(horizontalPanel_1);
		horizontalPanel_1.setSize("100%", "100%");

		horizontalPanel_1.add(verticalPanel_2);
		verticalPanel_2.setSize("217px", "100%");
		verticalPanel_2.add(tecnicosTable);
		tecnicosTable.setSize("237px", "33px");

		tecnicosTable.setHTML(0, 0, "<h3>Nombre</h3>");
		tecnicosTable.setHTML(0, 1, "<h3>Telefono</h3>");
		tecnicosTable.setHTML(0, 2, "<h3>Visible</h3>");
		tecnicosTable.setHTML(0, 3, "<h3>Procedimientos</h3>");
		verticalPanel_2.add(showTech);
		tecHorizontalPanel
				.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		horizontalPanel_1.add(tecHorizontalPanel);
		tecHorizontalPanel.setSize("50%", "100%");
		tecHorizontalPanel.add(techFrom);
		techFrom.setAnimationEnabled(true);

		techFrom.setWidget(verticalPanel);
		verticalPanel.setSize("100%", "100%");
		verticalPanel.add(tecName);
		nameTextBox.setText("");
		techFrom.setVisible(false);

		verticalPanel.add(nameTextBox);
		verticalPanel.add(techPass);

		verticalPanel.add(passwordTextBox);

		verticalPanel.add(tecTelf);

		verticalPanel.add(telftextBox_2);

		verticalPanel.add(tecProcLabel);

		verticalPanel.add(ProcTecCheckBoxPanel);
		ProcTecCheckBoxPanel.setWidth("100%");

		verticalPanel.add(horizontalPanel_2);
		horizontalPanel_2.setSize("100%", "100%");
		horizontalPanel_2.add(Añadir);
		techClose.setText("Cerrar");

		horizontalPanel_2.add(techClose);
		horizontalPanel_2.setCellVerticalAlignment(techClose,
				HasVerticalAlignment.ALIGN_MIDDLE);
		horizontalPanel_2.setCellHorizontalAlignment(techClose,
				HasHorizontalAlignment.ALIGN_RIGHT);

		tabLayoutPanel.add(vPanelProcedimientos, "Procedimientos", false);
		vPanelProcedimientos.setSize("100%", "100%");

		vPanelProcedimientos.add(horizontalPanel);
		horizontalPanel.setSize("100%", "100%");

		horizontalPanel.add(procVerticalPanel_2);
		procVerticalPanel_2.setSize("236px", "50%");
		procVerticalPanel_2.add(procedimientosTable);
		procVerticalPanel_2.setCellHorizontalAlignment(procedimientosTable,
				HasHorizontalAlignment.ALIGN_CENTER);
		procedimientosTable.setHTML(0, 0, "<h3>Codigo</h3>");
		procedimientosTable.setHTML(0, 1, "<h3>Nombre</h3>");
		procedimientosTable.setHTML(0, 2, "<h3>Descripcion</h3>");
		procedimientosTable.setSize("237px", "33px");
		procVerticalPanel_2.add(procFormButton);
		procFormHorizontal
				.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		horizontalPanel.add(procFormHorizontal);
		horizontalPanel.setCellWidth(procFormHorizontal, "50%");
		procFormHorizontal.setSize("50%", "230px");
		procFormHorizontal.add(procFrom);
		procFrom.setAnimationEnabled(true);
		procFrom.setVisible(false);

		procFrom.setWidget(verticalPanel_1);
		verticalPanel_1.setSize("100%", "100%");

		verticalPanel_1.add(label);
		procTextBox.setText("");

		verticalPanel_1.add(procTextBox);

		verticalPanel_1.add(lblCodigo);

		verticalPanel_1.add(procCodigoBox);

		verticalPanel_1.add(procDescLabel);

		verticalPanel_1.add(procDescTextArea);

		verticalPanel_1.add(horizontalPanel_3);
		verticalPanel_1.setCellHorizontalAlignment(horizontalPanel_3,
				HasHorizontalAlignment.ALIGN_CENTER);
		verticalPanel_1.setCellVerticalAlignment(horizontalPanel_3,
				HasVerticalAlignment.ALIGN_MIDDLE);
		horizontalPanel_3.setSize("183px", "26px");
		verticalPanel_1.setCellHeight(horizontalPanel_3, "100%");
		verticalPanel_1.setCellWidth(horizontalPanel_3, "100%");
		horizontalPanel_3.add(procButton);

		horizontalPanel_3.add(procCerrar);

		tabLayoutPanel.setSize("620px", "411px");

		tabLayoutPanel.add(vPanelProductos, "Productos", false);
		vPanelProductos.setSize("100%", "100%");

		vPanelProductos.add(horizontalPanel_4);
		horizontalPanel_4.setSize("100%", "100%");

		horizontalPanel_4.add(verticalPanel_3);

		verticalPanel_3.add(productosTable);
		addProductButton.setText("Añadir Producto");

		productosTable.setHTML(0, 0, "<h3>Codigo</h3>");
		productosTable.setHTML(0, 1, "<h3>Nombre</h3>");
		productosTable.setHTML(0, 2, "<h3>Descripcion</h3>");
		productosTable.setHTML(0, 3, "<h3>Procedimientos</h3>");

		verticalPanel_3.add(addProductButton);

		horizontalPanel_4.add(prodFormHorizontal);

		prodFormHorizontal.add(prodFrom);
		prodFormHorizontal.setCellHorizontalAlignment(prodFrom,
				HasHorizontalAlignment.ALIGN_RIGHT);
		prodFrom.setAnimationEnabled(true);

		prodFrom.setWidget(verticalPanel_4);
		verticalPanel_4.setSize("100%", "100%");

		verticalPanel_4.add(prodNombreLabel);
		prodNombreTextBox.setText("");

		verticalPanel_4.add(prodNombreTextBox);

		verticalPanel_4.add(prodCodigoLabel);

		verticalPanel_4.add(prodCodigoTextBox);
		prodDescripcionLabel.setDirectionEstimator(true);

		verticalPanel_4.add(prodDescripcionLabel);

		verticalPanel_4.add(prodTextArea);

		verticalPanel_4.add(productoProcedimientoLabel);

		verticalPanel_4.add(productoProcedimientoPanel);

		verticalPanel_4.add(horizontalPanel_5);
		horizontalPanel_5.setSize("183px", "26px");

		horizontalPanel_5.add(prodAddButton);

		horizontalPanel_5.add(prodCloseButton);
		prodFrom.setVisible(false);
		initWidget(tabLayoutPanel);
	}

	public HasClickHandlers getButtonTecnicos() {
		return showTech;
	}

	public HasClickHandlers getButtonFrom() {
		return Añadir;
	}

	public HasClickHandlers getTecCloseButton() {
		return techClose;
	}

	public void visibleAddTecButton(Boolean b) {
		showTech.setEnabled(b);
	}

	public void printTecnicos(Map<TecnicoDTO, List<ProcedimientoDTO>> map) {
		for (TecnicoDTO tecnico : map.keySet()) {
			tecnicosTable.setText(teci, 0, tecnico.getNombre());
			tecnicosTable.setText(teci, 1, tecnico.getTelefono());
			tecnicosTable.setText(teci, 2, tecnico.getVisible().toString());

			if (map.get(tecnico) != null) {
				String procedimientos = "";
				for (ProcedimientoDTO proc : map.get(tecnico)) {
					procedimientos += proc.getCodigo() + " ";
				}
				tecnicosTable.setText(teci, 3, procedimientos);
			} else {
				tecnicosTable.setText(teci, 3, "---");
			}
			teci++;
		}
	}

	public void visibleFromTecnico(Boolean b) {
		techFrom.setVisible(b);

	}

	public TecnicoDTO getNewTec() {
		TecnicoDTO res = new TecnicoDTO(nameTextBox.getText(),
				telftextBox_2.getText(), true);
		return res;
	}

	public void addTecTable(TecnicoDTO tec,
			List<ProcedimientoDTO> procedimientos) {

		tecnicosTable.setText(teci, 0, tec.getNombre());
		tecnicosTable.setText(teci, 1, tec.getTelefono());
		tecnicosTable.setText(teci, 2, tec.getVisible().toString());
		String procedimientosS = "";
		if (procedimientos != null) {
			for (ProcedimientoDTO proc : procedimientos) {
				procedimientosS += proc.getCodigo() + " ";
			}
			tecnicosTable.setText(teci, 3, procedimientosS);
		} else {
			tecnicosTable.setText(teci, 3, "---");
		}

		teci++;
	}

	public void addProcedimientosTec(List<String> codigos) {
		for (String s : codigos) {
			ProcTecCheckBoxPanel.add(new CheckBox(s));
		}

	}

	public List<String> getProcedimientosTecnico() {
		List<String> res = new LinkedList<String>();
		for (int i = 0; i < ProcTecCheckBoxPanel.getWidgetCount(); i++) {
			CheckBox box = (CheckBox) ProcTecCheckBoxPanel.getWidget(i);
			if (box.getValue()) {
				res.add(box.getHTML());
			}
		}
		
		return res;
	}

	// PROCEDIMIENTOS

	public HasClickHandlers getButtonProcs() {
		return procFormButton;
	}

	public HasClickHandlers getButtonProcFrom() {
		return procButton;
	}

	public HasClickHandlers getProcCloseButton() {
		return procCerrar;
	}

	public void visibleAddProcButton(Boolean b) {
		procFormButton.setEnabled(b);
	}

	public void visibleFromProc(Boolean b) {
		procFrom.setVisible(b);

	}

	public void printProcedimientos(List<ProcedimientoDTO> procedimientos) {
		for (ProcedimientoDTO procedimiento : procedimientos) {
			procedimientosTable.setText(proceci, 0, procedimiento.getCodigo());
			procedimientosTable.setText(proceci, 1, procedimiento.getNombre());
			procedimientosTable.setText(proceci, 2,
					procedimiento.getDescripcion());
			proceci++;
		}
	}

	public ProcedimientoDTO getNewProc() {
		return new ProcedimientoDTO(procTextBox.getText(),
				procCodigoBox.getText(), procDescTextArea.getText());
	}

	public void addProcTable(ProcedimientoDTO proc) {
		procedimientosTable.setText(proceci, 0, proc.getNombre());
		procedimientosTable.setText(proceci, 1, proc.getCodigo());
		procedimientosTable.setText(proceci, 2, proc.getDescripcion());
		proceci++;
	}

	// PRODUCTOS

	public HasClickHandlers getPordAddButton() {
		return addProductButton;
	}

	public HasClickHandlers getButtonProdFrom() {
		return prodAddButton;
	}

	public HasClickHandlers getProdCloseButton() {
		return prodCloseButton;
	}

	public void visibleAddProdButton(Boolean b) {
		addProductButton.setEnabled(b);
	}

	public void visibleFromProd(Boolean b) {
		prodFrom.setVisible(b);

	}

	public void printProductos(Map<ProductoDTO, List<ProcedimientoDTO>> map) {
		for (ProductoDTO producto : map.keySet()) {
			productosTable.setText(produci, 0, producto.getCodigo());
			productosTable.setText(produci, 1, producto.getNombre());
			productosTable.setText(produci, 2, producto.getDescripcion());
			
			if (map.get(producto) != null) {
				String procedimientos = "";
				for (ProcedimientoDTO proc : map.get(producto)) {
					procedimientos += proc.getCodigo() + " ";
				}
				productosTable.setText(teci, 3, procedimientos);
			} else {
				productosTable.setText(teci, 3, "---");
			}
			produci++;
		}
	}

	public ProductoDTO getNewProd() {
		return new ProductoDTO(prodCodigoTextBox.getText(),
				prodNombreTextBox.getText(), prodTextArea.getText());
	}

	public void addProdTable(ProductoDTO prod, List<ProcedimientoDTO> procedimientos) {
		productosTable.setText(proceci, 0, prod.getCodigo());
		productosTable.setText(proceci, 1, prod.getNombre());
		productosTable.setText(proceci, 2, prod.getDescripcion());
		
		String procedimientosS = "";
		if (procedimientos != null) {
			for (ProcedimientoDTO proc : procedimientos) {
				procedimientosS += proc.getCodigo() + " ";
			}
			productosTable.setText(proceci, 3, procedimientosS);
		} else {
			productosTable.setText(proceci, 3, "---");
		}
		produci++;
	}

	public String getTecPass() {
		return passwordTextBox.getText();
	}

	public void addProcedimientosProd(List<String> codigos) {
		for (String s : codigos) {
			productoProcedimientoPanel.add(new CheckBox(s));
		}

	}

	public List<String> getProcedimientosProducto() {
		List<String> res = new LinkedList<String>();
		for (int i = 0; i < productoProcedimientoPanel.getWidgetCount(); i++) {
			CheckBox box = (CheckBox) productoProcedimientoPanel.getWidget(i);
			if (box.getValue()) {
				res.add(box.getHTML());
			}
		}

		return res;
	}

}
