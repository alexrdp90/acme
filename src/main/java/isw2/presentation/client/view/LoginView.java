package isw2.presentation.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;

import isw2.presentation.client.AcmeConstants;
import isw2.presentation.client.presenter.*;

public class LoginView extends Composite implements LoginPresenter.Display {

	private AcmeConstants constants = GWT.create(AcmeConstants.class);
	
	private VerticalPanel mainPanel = new VerticalPanel();
	private Label labelNombre = new Label(constants.nombreUsuario());
	private TextBox nombreTecnico = new TextBox();
	private Label labelpass = new Label(constants.password());
	private PasswordTextBox passTecnico = new PasswordTextBox();
	private Button sendButton = new Button(constants.enviar());

	private DialogBox dialogBox = new DialogBox();
	private VerticalPanel dialogVPanel = new VerticalPanel();
	private Label dialogText = new Label();
	private Button closeButton = new Button(constants.cerrar());
	
	public LoginView() {
		initWidget(dialogBox);
		dialogVPanel.add(labelNombre);
		dialogVPanel.add(nombreTecnico);
		dialogVPanel.add(labelpass);
		dialogVPanel.add(passTecnico);
		dialogVPanel.add(sendButton);
		dialogBox.add(dialogVPanel);
	}

	public HasClickHandlers getCloseButton() {
		return closeButton;
	}

	public HasClickHandlers getSendButton() {
		return sendButton;
	}

	public String getNombre() {
		return nombreTecnico.getText();
	}

	public String getPass() {
		return passTecnico.getText();
	}

	public void success(String result) {
		dialogText.setText(result);
		dialogBox.center();
		closeButton.setFocus(true);
		mainPanel.setVisible(false);
	}

	public void error(String result) {
		dialogText.setText("Algo fue mal");
		dialogBox.center();
		closeButton.setFocus(true);
	}

	public void closeEvent() {
		dialogBox.hide();
		sendButton.setEnabled(true);
		nombreTecnico.setFocus(true);
	}

}
