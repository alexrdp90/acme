package isw2.presentation.client.view;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.DatePicker;

import isw2.presentation.client.AcmeConstants;
import isw2.presentation.client.presenter.*;
import isw2.presentation.shared.IncidenciaDTO;
import isw2.presentation.shared.ProcedimientoDTO;

public class ClienteView extends Composite implements ClientePresenter.Display {

	private AcmeConstants constants = GWT.create(AcmeConstants.class);

	private VerticalPanel mainPanel = new VerticalPanel();
	private List<IncidenciaDTO> listIncidencias;

	private final TextBox nombreCliente = new TextBox();
	private final TextBox emailCliente = new TextBox();
	private final TextBox descripcion = new TextBox();
	private final DatePicker fechaCompra = new DatePicker();
	private final ListBox listboxProductos = new ListBox();
	private final ListBox listboxProcedimientos = new ListBox();

	private final Button sendButton = new Button(constants.enviar());
	private final Label lblNewLabel = new Label(constants.nombre() + ":");
	private final Label lblNewLabel_1 = new Label(constants.email() + ":");
	private final Label lblNewLabel_2 = new Label(constants.descripcion() + ":");
	private final Label lblNewLabel_3 = new Label(constants.fechaCompra() + ":");
	private final Label lblNewLabel_4 = new Label(constants.producto() + ":");
	private final Label lblNewLabel_5 = new Label(constants.procedimiento()
			+ ":");
	private final Label incidenciaslabel = new Label(
			constants.incidenciasRelacionadas() + ":");
	private final VerticalPanel incidenciasPanel = new VerticalPanel();

	public ClienteView() {
		initWidget(mainPanel);

		mainPanel.add(lblNewLabel);
		mainPanel.add(nombreCliente);
		mainPanel.add(lblNewLabel_1);
		mainPanel.add(emailCliente);
		mainPanel.add(lblNewLabel_2);
		mainPanel.add(descripcion);
		mainPanel.add(lblNewLabel_3);
		mainPanel.add(fechaCompra);
		mainPanel.add(lblNewLabel_4);
		mainPanel.add(listboxProductos);
		mainPanel.add(lblNewLabel_5);
		mainPanel.add(listboxProcedimientos);
		mainPanel.add(incidenciaslabel);
		mainPanel.add(incidenciasPanel);
		mainPanel.add(sendButton);
		mainPanel.add(sendButton);
	}

	public HasClickHandlers getSendButton() {
		return sendButton;
	}

	public String getCliente() {
		return nombreCliente.getText();
	}

	public String getEmail() {
		return emailCliente.getText();
	}

	public String getDescripcion() {
		return descripcion.getText();
	}

	public Date getFechaCompra() {
		return fechaCompra.getHighlightedDate();
	}

	public ListBox getListBoxProductos() {
		return listboxProductos;
	}

	public Integer getIndiceProducto() {
		return listboxProductos.getSelectedIndex();
	}

	public Integer getIndiceProcedimiento() {
		return listboxProcedimientos.getSelectedIndex();
	}

	public void addIncidenciasCheck(List<IncidenciaDTO> listaIncidencia) {
		listIncidencias = listaIncidencia;
		for (IncidenciaDTO s : listaIncidencia) {
			incidenciasPanel.add(new CheckBox(s.toString()));
		}
	}

	public void addProcedimientos(List<ProcedimientoDTO> lista) {
		listboxProcedimientos.clear();
		int index = 0;
		for (ProcedimientoDTO s : lista) {
			listboxProcedimientos.insertItem(s.toString(), index);
			index++;
		}
	}

	public List<IncidenciaDTO> getIncidencias() {
		List<IncidenciaDTO> res = new LinkedList<IncidenciaDTO>();
		for (int i = 0; i < incidenciasPanel.getWidgetCount(); i++) {
			CheckBox box = (CheckBox) incidenciasPanel.getWidget(i);
			if (box.getValue()) {
				res.add(listIncidencias.get(i));
			}
		}
		return res;
	}

}
