package isw2.presentation.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;

import isw2.presentation.client.AcmeConstants;
import isw2.presentation.client.presenter.TecnicoPresenter;

public class TecnicoView extends Composite implements TecnicoPresenter.Display {

	private AcmeConstants constants = GWT.create(AcmeConstants.class);

	private final VerticalPanel mainPanel = new VerticalPanel();
	private final Label reclamarLabel = new Label(constants.reclamar());
	private final ListBox reclamarListBox = new ListBox();
	private final Button reclamarButton = new Button(constants.enviar());
	private final Label resolverLabel = new Label(constants.resolver());
	private final ListBox resolverListBox = new ListBox();
	private final Button resolverButton = new Button(constants.enviar());
	private final Label lblNewLabel = new Label(constants.respuesta());
	private final TextArea resTextArea = new TextArea();

	public TecnicoView() {
		initWidget(mainPanel);
		mainPanel.add(reclamarLabel);
		mainPanel.add(reclamarListBox);
		mainPanel.add(reclamarButton);
		mainPanel.add(resolverLabel);
		mainPanel.add(resolverListBox);
		mainPanel.add(lblNewLabel);
		mainPanel.add(resTextArea);
		mainPanel.add(resolverButton);
		RootPanel.get("content").add(mainPanel);
	}

	public Integer getReclamarIncidencia() {
		return reclamarListBox.getSelectedIndex();
	}

	public Integer getResolverInidencia() {
		return resolverListBox.getSelectedIndex();
	}

	public HasClickHandlers getReclamarIncidenciaButton() {
		return reclamarButton;
	}

	public HasClickHandlers getResolverInidenciaButton() {
		return resolverButton;
	}

	public ListBox getListbox() {
		return reclamarListBox;
	}

	public ListBox getListboxReclamadas() {
		return resolverListBox;
	}

	public String getRespuesta() {
		return resTextArea.getText();
	}
}
