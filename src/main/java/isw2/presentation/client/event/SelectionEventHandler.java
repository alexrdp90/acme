package isw2.presentation.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface SelectionEventHandler extends EventHandler {
	void select(SelectionEvent event);

}
