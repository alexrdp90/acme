package isw2.presentation.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class SelectionEvent extends GwtEvent<SelectionEventHandler>{

	public static Type<SelectionEventHandler> TYPE = new Type<SelectionEventHandler>();
	private String seleccion;
	
	public SelectionEvent(String seleccion){
		this.seleccion=seleccion;
	}
	protected void dispatch(SelectionEventHandler handler) {
		handler.select(this);
		
	}

	public com.google.gwt.event.shared.GwtEvent.Type<SelectionEventHandler> getAssociatedType() {
		return TYPE;
	}
	
	public String getSelection(){
		return seleccion;
	}

}
