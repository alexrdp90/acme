package isw2.presentation.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface RegisterServiceAsync {

	void register(String nombre, String password,
			AsyncCallback<String> callback);

}
