package isw2.presentation.client;

import isw2.presentation.shared.ProcedimientoDTO;
import isw2.presentation.shared.ProductoDTO;
import isw2.presentation.shared.TecnicoDTO;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface AdminServiceAsync {

	void añadirProcedimiento(String codigo, String nombre, String descripcion,
			AsyncCallback<ProcedimientoDTO> callback);

	void añadirProducto(String codigo, String nombre, String descripcion,
			List<String> procedimientos, AsyncCallback<ProductoDTO> callback);

	void añadirTecnico(String nombre, String pass, String telefono,
			List<String> procedimientos, AsyncCallback<TecnicoDTO> callback);

	void listarProcedimientos(AsyncCallback<List<ProcedimientoDTO>> callback);

	void listarProductos(AsyncCallback<List<ProductoDTO>> callback);

	void listarTecnicos(AsyncCallback<List<TecnicoDTO>> callback);

}
