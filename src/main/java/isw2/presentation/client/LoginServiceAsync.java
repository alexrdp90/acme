package isw2.presentation.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface LoginServiceAsync {

	void login(String nombre, String pass, AsyncCallback<Boolean> callback);

}
