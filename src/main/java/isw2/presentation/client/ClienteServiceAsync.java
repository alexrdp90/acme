package isw2.presentation.client;

import isw2.presentation.shared.IncidenciaDTO;
import isw2.presentation.shared.ProcedimientoDTO;
import isw2.presentation.shared.ProductoDTO;

import java.util.Date;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface ClienteServiceAsync {

	void registrarIncidencia(String cliente, String email, String descripcion,
			Date fechaCompra, String codigoProducto,
			String codigoProcedimiento, List<Integer> idIncidencias,
			AsyncCallback<String> callback);

	void visualizarIncidencia(Integer idIncidencia,
			AsyncCallback<String> callback);

	void listarProductos(AsyncCallback<List<ProductoDTO>> callback);

	void listarProcedimientos(AsyncCallback<List<ProcedimientoDTO>> callback);

	void listarIncidencias(AsyncCallback<List<IncidenciaDTO>> callback);

	void listarProcedimientos(String codigoProducto,
			AsyncCallback<List<ProcedimientoDTO>> callback);

}
