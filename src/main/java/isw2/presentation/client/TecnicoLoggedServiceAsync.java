package isw2.presentation.client;

import isw2.presentation.shared.IncidenciaDTO;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface TecnicoLoggedServiceAsync {

	void reclamarIncidencia(Integer idIncidencia, AsyncCallback<String> callback);

	void listarIncidencias(AsyncCallback<List<IncidenciaDTO>> callback);

	void listarIncidenciasReclamadas(AsyncCallback<List<IncidenciaDTO>> callback);

	void addRespuesta(String res, Integer idIncidencia,
			AsyncCallback<Void> callback);

}
