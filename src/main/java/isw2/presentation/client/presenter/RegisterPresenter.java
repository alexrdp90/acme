package isw2.presentation.client.presenter;

import isw2.presentation.client.RegisterService;
import isw2.presentation.client.RegisterServiceAsync;
import isw2.presentation.client.view.RegisterView;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;

public class RegisterPresenter implements Presenter {

	private final Display display;
	private RegisterServiceAsync rpcService;
	@SuppressWarnings("unused")
	private HandlerManager eventBus;

	public interface Display {
		String getNombre();
		String getPassword();
		HasClickHandlers getSendButton();
		void show(String message);
	}

	public RegisterPresenter(HandlerManager eventBus) {
		this.rpcService = GWT.create(RegisterService.class);
		this.eventBus = eventBus;
		this.display = new RegisterView();
	}

	public void bind() {
		display.getSendButton().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				checkLogin();
			}
		});
	}

	protected void checkLogin() {
		String nombre = display.getNombre();
		String password = display.getPassword();

		rpcService.register(nombre, password, new AsyncCallback<String>() {
			public void onSuccess(String result) {
				display.show(result);
			}
			public void onFailure(Throwable arg) {
				display.show("Login incorrecto");
			}
		});
	}

	public void go(HasWidgets container) {
		bind();
//		container.clear();
//		container.add(display.asWidget());
	}

}
