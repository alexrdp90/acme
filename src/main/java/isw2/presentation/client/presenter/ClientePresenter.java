package isw2.presentation.client.presenter;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import isw2.presentation.client.ClienteService;
import isw2.presentation.client.ClienteServiceAsync;
import isw2.presentation.client.view.ClienteView;
import isw2.presentation.shared.IncidenciaDTO;
import isw2.presentation.shared.ProcedimientoDTO;
import isw2.presentation.shared.ProductoDTO;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

public class ClientePresenter implements Presenter {

	private final ClienteServiceAsync rpcService;
	@SuppressWarnings("unused")
	private final HandlerManager eventBus;
	private final Display display;

	private List<ProductoDTO> productos;
	private List<ProcedimientoDTO> procedimientos;

	public interface Display {
		Widget asWidget();

		HasClickHandlers getSendButton();

		String getCliente();

		String getEmail();

		String getDescripcion();

		Date getFechaCompra();

		ListBox getListBoxProductos();

		Integer getIndiceProducto();

		Integer getIndiceProcedimiento();

		void addIncidenciasCheck(List<IncidenciaDTO> listaIncidencia);
		
		void addProcedimientos(List<ProcedimientoDTO> lista);

		List<IncidenciaDTO> getIncidencias();

	}

	public ClientePresenter(HandlerManager eventBus) {
		ClienteServiceAsync rpcService = GWT.create(ClienteService.class);
		this.rpcService = rpcService;
		this.eventBus = eventBus;
		this.display = new ClienteView();
	}

	public void go(HasWidgets container) {
		bind();

		rpcService.listarProductos(new AsyncCallback<List<ProductoDTO>>() {
			public void onFailure(Throwable caught) {
				Window.alert("Algo fue mal");
			}

			public void onSuccess(List<ProductoDTO> result) {
				productos = result;
				ListBox lb = display.getListBoxProductos();
				int i = 0;
				for (ProductoDTO producto : result) {
					lb.insertItem(producto.toString(), i);
					i++;
				}
			}
		});

		rpcService.listarIncidencias(new AsyncCallback<List<IncidenciaDTO>>() {
			public void onFailure(Throwable caught) {
				Window.alert("Algo fue mal");
			}

			public void onSuccess(List<IncidenciaDTO> result) {
				display.addIncidenciasCheck(result);

			}
		});

		container.clear();
		container.add(display.asWidget());
	}

	public void bind() {
		display.getSendButton().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent arg0) {
				registrarIncidencia();
			}
		});
		display.getListBoxProductos().addChangeHandler(new ChangeHandler() {
			public void onChange(ChangeEvent event) {
				actualizarProcedimientos();
			}
		});
	}
	
	private void actualizarProcedimientos(){
		int indexProd = display.getIndiceProducto();

		if (indexProd == -1) {
			Window.alert("Seleccione un producto");
			return;
		}
		String codigoProducto = productos.get(indexProd).getCodigo();
		rpcService.listarProcedimientos(codigoProducto, new AsyncCallback<List<ProcedimientoDTO>>() {
			public void onFailure(Throwable caught) {
				Window.alert("Algo fue mal");
			}
			public void onSuccess(List<ProcedimientoDTO> result) {
				procedimientos = result;
				display.addProcedimientos(result);
			}
		});
	}

	private void registrarIncidencia() {
		String cliente = display.getCliente();
		String email = display.getEmail();
		String descripcion = display.getDescripcion();
		Date fechaCompra = display.getFechaCompra();

		String codigoProducto, codigoProcedimiento;

		int indexProd = display.getIndiceProducto();
		int indexProc = display.getIndiceProcedimiento();

		if (indexProc == -1 || indexProd == -1) {
			Window.alert("Seleccione un producto y un procedimiento");
			return;
		} else if (indexProc == procedimientos.size()) {
			codigoProducto = null;
		} else {
			codigoProducto = productos.get(indexProd).getCodigo();
		}
		codigoProcedimiento = procedimientos.get(indexProc).getCodigo();

		List<Integer> idIncidencias = new LinkedList<Integer>();
		for (IncidenciaDTO inc : display.getIncidencias()) {
			idIncidencias.add(inc.getId());
		}

		rpcService.registrarIncidencia(cliente, email, descripcion,
				fechaCompra, codigoProducto, codigoProcedimiento,
				idIncidencias, new AsyncCallback<String>() {

					public void onFailure(Throwable caught) {
						Window.alert("Algo fue mal");
					}

					public void onSuccess(String result) {
						Window.alert(result);
					}
				});
	}

}
