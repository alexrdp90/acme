package isw2.presentation.client.presenter;

import java.util.List;

import isw2.presentation.client.TecnicoLoggedService;
import isw2.presentation.client.TecnicoLoggedServiceAsync;
import isw2.presentation.client.view.TecnicoView;
import isw2.presentation.shared.IncidenciaDTO;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

public class TecnicoPresenter implements Presenter {

	@SuppressWarnings("unused")
	private final HandlerManager eventBus;
	private final Display display;
	private final TecnicoLoggedServiceAsync rpc;
	private List<IncidenciaDTO> incidenciasDisponibles;
	private List<IncidenciaDTO> incidenciasReclamadas;

	public interface Display {
		Widget asWidget();

		Integer getReclamarIncidencia();

		Integer getResolverInidencia();

		HasClickHandlers getReclamarIncidenciaButton();

		HasClickHandlers getResolverInidenciaButton();

		ListBox getListbox();

		ListBox getListboxReclamadas();
		
		 String getRespuesta();
	}

	public TecnicoPresenter(HandlerManager eventBus) {
		TecnicoLoggedServiceAsync rpc = GWT.create(TecnicoLoggedService.class);
		this.eventBus = eventBus;
		this.display = new TecnicoView();
		this.rpc = rpc;

	}

	public void go(HasWidgets container) {
		bind();
		rpc.listarIncidencias(new AsyncCallback<List<IncidenciaDTO>>() {
			public void onFailure(Throwable caught) {
				Window.alert("Algo fue mal");
			}

			public void onSuccess(List<IncidenciaDTO> result) {
				incidenciasDisponibles = result;
				int i = 0;
				ListBox lb = display.getListbox();
				lb.clear();
				for (IncidenciaDTO inc : incidenciasDisponibles) {
					lb.insertItem(inc.toString(), i);
					i++;
				}
			}
		});
		listarIncidenciasReclamadas();
		container.clear();
		container.add(display.asWidget());
	}

	private void listarIncidenciasReclamadas() {
		rpc.listarIncidenciasReclamadas(new AsyncCallback<List<IncidenciaDTO>>() {
			public void onFailure(Throwable caught) {
				Window.alert("Algo fue mal");
			}

			public void onSuccess(List<IncidenciaDTO> result) {
				incidenciasReclamadas = result;
				int i = 0;
				ListBox lb = display.getListboxReclamadas();
				lb.clear();
				for (IncidenciaDTO inc : incidenciasReclamadas) {
					lb.insertItem(inc.toString(), i);
					i++;
				}
			}
		});
	}

	public void bind() {
		display.getReclamarIncidenciaButton().addClickHandler(
				new ClickHandler() {
					public void onClick(ClickEvent arg0) {
						reclamarIncidencia();
					}
				});
		display.getResolverInidenciaButton().addClickHandler(
				new ClickHandler() {
					public void onClick(ClickEvent event) {
						añadirRespuesta();
						//resolverIncidencia();
					}
				});
	}
	
	private void añadirRespuesta(){
		String res =  display.getRespuesta();
		rpc.addRespuesta(res,incidenciasDisponibles.get(display.getResolverInidencia()).getId(), new AsyncCallback<Void>() {
			public void onFailure(Throwable caught) {
				Window.alert("Algo fue mal");
			}
			public void onSuccess(Void result) {
				Window.alert("Incidencia resuelta con exito");
				
			}
		});
		
		
	}

	private void reclamarIncidencia() {
		int index = display.getReclamarIncidencia();
		if (index == -1) {
			Window.alert("Seleccione una incidencia");
			return;
		}
		Integer idIncidencia = incidenciasDisponibles.get(index).getId();
		rpc.reclamarIncidencia(idIncidencia, new AsyncCallback<String>() {
			public void onFailure(Throwable caught) {
				Window.alert("Algo fue mal");
			}

			public void onSuccess(String result) {
				Window.alert(result);
				listarIncidenciasReclamadas();
			}
		});
	}

}
