package isw2.presentation.client.presenter;

import isw2.presentation.client.AcmeMessages;
import isw2.presentation.client.LoginService;
import isw2.presentation.client.LoginServiceAsync;
import isw2.presentation.client.event.SelectionEvent;
import isw2.presentation.client.view.LoginView;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;

public class LoginPresenter implements Presenter {

	private final LoginServiceAsync rpcService;
	private final HandlerManager eventBus;
	private final Display display;
	
	private AcmeMessages messages;

	public interface Display {
		Widget asWidget();

		HasClickHandlers getCloseButton();

		HasClickHandlers getSendButton();

		String getNombre();

		String getPass();

		void success(String result);

		void error(String result);

		void closeEvent();
	}

	public LoginPresenter(HandlerManager eventBus) {
		this.rpcService = GWT.create(LoginService.class);;
		this.eventBus = eventBus;
		this.display = new LoginView();
		this.messages = GWT.create(AcmeMessages.class);
	}

	public void bind() {
		display.getCloseButton().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent arg0) {
				display.closeEvent();
			}
		});
		display.getSendButton().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				checkLogin();
			}
		});
	}

	protected void checkLogin() {
		String nombre = display.getNombre();
		String pass = display.getPass();
		rpcService.login(nombre, pass, new AsyncCallback<Boolean>() {
			public void onSuccess(Boolean result) {
				if(result == true){
					Window.alert(messages.correctLogin());
					eventBus.fireEvent(new SelectionEvent("tecnico"));
				} else {
					Window.alert(messages.incorrectLogin());
				}
			}

			public void onFailure(Throwable arg0) {
				Window.alert("Algo fue mal");
			}
		});
	}

	public void go(HasWidgets container) {
		bind();
		container.clear();
		container.add(display.asWidget());
	}

}
