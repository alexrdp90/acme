package isw2.presentation.client.presenter;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import isw2.presentation.client.AcmeMessages;
import isw2.presentation.client.AdminService;
import isw2.presentation.client.AdminServiceAsync;
import isw2.presentation.client.view.AdminView;
import isw2.presentation.shared.FieldVerifier;
import isw2.presentation.shared.ProductoDTO;
import isw2.presentation.shared.TecnicoDTO;
import isw2.presentation.shared.ProcedimientoDTO;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;

public class AdminPresenter implements Presenter {

	private AcmeMessages messages = GWT.create(AcmeMessages.class);

	@SuppressWarnings("unused")
	private final HandlerManager eventBus;
	private final Display display;
	private final AdminServiceAsync rpc;

	public interface Display {
		Widget asWidget();

		String getNombreTecnico();

		String getContraseñaTecnico();

		String getTelefonoTecnico();

		String getCodigoProcedimiento();

		String getNombreProcedimiento();

		String getDescripcionProcedimiento();

		String getCodigoProducto();

		String getNombreProducto();

		String getDescripcionProducto();

		void printTecnicos(Map<TecnicoDTO, List<ProcedimientoDTO>> tecnicos);

		HasClickHandlers getButtonTecnicos();

		void visibleFromTecnico(Boolean b);

		HasClickHandlers getButtonFrom();

		TecnicoDTO getNewTec();

		void addTecTable(TecnicoDTO tec, List<ProcedimientoDTO> procedimientos);

		void visibleAddTecButton(Boolean b);

		HasClickHandlers getButtonProcs();

		HasClickHandlers getButtonProcFrom();

		void visibleAddProcButton(Boolean b);

		void visibleFromProc(Boolean b);

		void printProcedimientos(List<ProcedimientoDTO> procedimientos);

		ProcedimientoDTO getNewProc();

		void addProcTable(ProcedimientoDTO proc);

		public HasClickHandlers getProcCloseButton();

		public HasClickHandlers getTecCloseButton();

		public String getTecPass();

		HasClickHandlers getPordAddButton();

		HasClickHandlers getButtonProdFrom();

		HasClickHandlers getProdCloseButton();

		void visibleAddProdButton(Boolean b);

		void visibleFromProd(Boolean b);

		void printProductos(Map<ProductoDTO, List<ProcedimientoDTO>> map);

		ProductoDTO getNewProd();

		void addProdTable(ProductoDTO prod,
				List<ProcedimientoDTO> procedimientos);

		void addProcedimientosTec(List<String> codigos);

		List<String> getProcedimientosTecnico();

		void addProcedimientosProd(List<String> codigos);

		List<String> getProcedimientosProducto();

	}

	public AdminPresenter(HandlerManager eventBus) {
		this.eventBus = eventBus;
		this.display = new AdminView();
		this.rpc = GWT.create(AdminService.class);

		printTecnicosDisplay();
		printProcedimientosDisplay();
		printProductosDisplay();
	}

	public void printTecnicosDisplay() {
		rpc.listarTecnicos(new AsyncCallback<List<TecnicoDTO>>() {

			public void onFailure(Throwable arg) {
				Window.alert("Ha fallado algo\n" + arg.toString());
			}

			public void onSuccess(List<TecnicoDTO> result) {
				buscaProcedimientoTecnico(result);

			}
		});

	}

	public Map<TecnicoDTO, List<ProcedimientoDTO>> buscaProcedimientoTecnico(
			List<TecnicoDTO> result) {
		Map<TecnicoDTO, List<ProcedimientoDTO>> res = new HashMap<TecnicoDTO, List<ProcedimientoDTO>>();
		for (final TecnicoDTO tec : result) {
			res.put(tec, new LinkedList<ProcedimientoDTO>());
			rpc.getProcedimientosTecnico(
					tec,
					res,
					new AsyncCallback<Map<TecnicoDTO, List<ProcedimientoDTO>>>() {

						public void onFailure(Throwable arg) {
							Window.alert("Ha fallado algo" + arg.toString());
						}

						public void onSuccess(
								Map<TecnicoDTO, List<ProcedimientoDTO>> res) {
							display.printTecnicos(res);
						}
					});
		}

		return res;
	}

	public void printProcedimientosDisplay() {
		rpc.listarProcedimientos(new AsyncCallback<List<ProcedimientoDTO>>() {
			public void onFailure(Throwable arg) {
				Window.alert("Ha fallado algo" + arg.toString());
			}

			public void onSuccess(List<ProcedimientoDTO> result) {
				display.printProcedimientos(result);
				List<String> codigos = new LinkedList<String>();
				for (ProcedimientoDTO proc : result) {
					codigos.add(proc.getCodigo());
				}
				display.addProcedimientosTec(codigos);
				display.addProcedimientosProd(codigos);
			}
		});
	}

	public void printProductosDisplay() {
		rpc.listarProductos(new AsyncCallback<List<ProductoDTO>>() {
			public void onFailure(Throwable arg) {
				Window.alert("Ha fallado algo" + arg.toString());
			}

			public void onSuccess(List<ProductoDTO> result) {
				buscaProcedimientoProducto(result);
			}
		});

	}

	public Map<ProductoDTO, List<ProcedimientoDTO>> buscaProcedimientoProducto(
			List<ProductoDTO> result) {
		Map<ProductoDTO, List<ProcedimientoDTO>> res = new HashMap<ProductoDTO, List<ProcedimientoDTO>>();
		for (ProductoDTO tec : result) {
			res.put(tec, new LinkedList<ProcedimientoDTO>());
			rpc.getProcedimientosProducto(
					tec,
					res,
					new AsyncCallback<Map<ProductoDTO, List<ProcedimientoDTO>>>() {

						public void onFailure(Throwable arg) {
							Window.alert("Ha fallado algo" + arg.toString());
						}

						public void onSuccess(
								Map<ProductoDTO, List<ProcedimientoDTO>> res) {
							display.printProductos(res);
						}
					});
		}

		return res;
	}

	public void bind() {
		display.getButtonTecnicos().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				display.visibleFromTecnico(true);
				display.visibleAddTecButton(false);
			}
		});

		display.getButtonFrom().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addTecDisplay(display.getNewTec());
				display.visibleFromTecnico(false);
				display.visibleAddTecButton(true);
			}
		});
		display.getTecCloseButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				display.visibleFromTecnico(false);
				display.visibleAddTecButton(true);

			}
		});

		// Procedimientos
		display.getButtonProcs().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				display.visibleFromProc(true);
				display.visibleAddProcButton(false);
			}
		});

		display.getButtonProcFrom().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addProcDisplay(display.getNewProc());
				display.visibleFromProc(false);
				display.visibleAddProcButton(true);
			}
		});

		display.getProcCloseButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				display.visibleFromProc(false);
				display.visibleAddProcButton(true);

			}
		});

		// PRODUCTOS

		display.getPordAddButton().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				display.visibleFromProd(true);
				display.visibleAddProdButton(false);
			}
		});

		display.getButtonProdFrom().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addProdDisplay(display.getNewProd());
				display.visibleFromProd(false);
				display.visibleAddProdButton(true);
			}
		});

		display.getProdCloseButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				display.visibleFromProd(false);
				display.visibleAddProdButton(true);

			}
		});

	}

	protected void addTecDisplay(TecnicoDTO tec) {
		String nombre = display.getNombreTecnico();
		String contraseña = display.getContraseñaTecnico();
		String telefono = display.getTelefonoTecnico();
		List<String> codigos = display.getProcedimientosTecnico();

		if (!FieldVerifier.isProcedimientosValid(codigos)) {
			Window.alert("Solo puede seleccionar 10 procedimientos");
			return;
		}

		rpc.añadirTecnico(nombre, contraseña, telefono, codigos,
				new AsyncCallback<TecnicoDTO>() {
					public void onFailure(Throwable caught) {
						Window.alert("Ha fallado algo:\n" + caught.toString());
					}

					public void onSuccess(TecnicoDTO result) {
						Window.alert(messages.altaTecnico(result.getNombre()));
					}
				});

	}

	protected void addProcDisplay(ProcedimientoDTO proc) {
		String codigo = display.getNombreProcedimiento();
		String nombre = display.getNombreProcedimiento();
		String descripcion = display.getDescripcionProcedimiento();

		rpc.añadirProcedimiento(codigo, nombre, descripcion,
				new AsyncCallback<ProcedimientoDTO>() {

					public void onFailure(Throwable caught) {
						Window.alert("Ha fallado algo:\n" + caught.toString());
					}

					public void onSuccess(ProcedimientoDTO result) {
						Window.alert(messages.altaProcedimiento(
								result.getCodigo(), result.getNombre()));

					}
				});

	}

	protected void addProdDisplay(ProductoDTO prod) {
		String codigo = display.getNombreProducto();
		String nombre = display.getNombreProducto();
		String descripcion = display.getDescripcionProducto();

		rpc.añadirProducto(codigo, nombre, descripcion,
				display.getProcedimientosProducto(),
				new AsyncCallback<ProductoDTO>() {

					public void onFailure(Throwable caught) {
						Window.alert("Ha fallado algo:\n" + caught.toString());
					}

					public void onSuccess(ProductoDTO result) {
						Window.alert(messages.altaProducto(result.getCodigo(),
								result.getNombre()));
					}
				});

	}

	public void go(HasWidgets container) {
		bind();
		container.clear();
		container.add(display.asWidget());
	}
}
