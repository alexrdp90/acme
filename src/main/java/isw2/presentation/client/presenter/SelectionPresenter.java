package isw2.presentation.client.presenter;

import isw2.presentation.client.event.SelectionEvent;
import isw2.presentation.client.view.SelectionView;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;

public class SelectionPresenter implements Presenter {

	private final HandlerManager eventBus;
	private final Display display;

	public interface Display {
		Widget asWidget();

		HasClickHandlers getAdminButton();

		HasClickHandlers getTecnicoButton();

		HasClickHandlers getClienteButton();
	}

	public SelectionPresenter(HandlerManager eventBus) {
		this.eventBus = eventBus;
		this.display = new SelectionView();
	}

	public void bind() {
		display.getAdminButton().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent arg0) {
				eventBus.fireEvent(new SelectionEvent("admin"));
			}
		});
		display.getTecnicoButton().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new SelectionEvent("login"));

			}
		});
		display.getClienteButton().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new SelectionEvent("cliente"));

			}
		});
	}

	public void go(HasWidgets container) {
		bind();
		container.clear();
		container.add(display.asWidget());
	}

}
