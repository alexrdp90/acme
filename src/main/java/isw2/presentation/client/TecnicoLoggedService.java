package isw2.presentation.client;

import isw2.presentation.shared.IncidenciaDTO;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("tecnico")
public interface TecnicoLoggedService extends RemoteService {
	String reclamarIncidencia(Integer idIncidencia) throws IllegalArgumentException;
	
	List<IncidenciaDTO> listarIncidencias();
	
	List<IncidenciaDTO> listarIncidenciasReclamadas();
	
	void addRespuesta(String res, Integer idIncidencia);
}
