package isw2.infrastructure;

import java.util.*;

import javax.persistence.EntityManager;
import isw2.domain.procedimiento.Procedimiento;
import isw2.domain.procedimiento.ProcedimientoImpl;
import isw2.domain.producto.Producto;
import isw2.domain.producto.ProductoImpl;
import isw2.domain.tecnico.Tecnico;
import isw2.domain.tecnico.TecnicoImpl;
import isw2.presentation.shared.ProcedimientoDTO;

public class ProcedimientoDAO {

	private EntityManager em;
	
	public ProcedimientoDAO(EntityManager em){
		this.em = em;
	}
	
	public void store(Procedimiento procedimiento) {
		em.persist(procedimiento);
	}

	public Procedimiento find(String id) {
		Procedimiento procedimiento = null;
		try {
			em.getTransaction().begin();
			procedimiento = em.find(ProcedimientoImpl.class, id);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return procedimiento;
	}

	public List<Procedimiento> findAll() {
		List<Procedimiento> result = new LinkedList<Procedimiento>();
		try {
			em.getTransaction().begin();
			result.addAll(em.createQuery("from ProcedimientoImpl",
					ProcedimientoImpl.class).getResultList());
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return result;
	}

	public void update(Procedimiento procedimiento) {
		try {
			em.getTransaction().begin();
			em.detach(procedimiento);
			em.merge(procedimiento);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
	}

	public List<ProcedimientoDTO> findAllDTOs() {
		List<Procedimiento> procedimientos = findAll();
		List<ProcedimientoDTO> result = new ArrayList<ProcedimientoDTO>();
		for (Procedimiento p : procedimientos) {
			result.add(new ProcedimientoDTO(p.getCodigo(), p.getNombre(), p
					.getDescripcion()));
		}
		return result;
	}

	public List<ProcedimientoDTO> findAllDTOs(String nombreTecnico) {
		List<ProcedimientoDTO> result = new LinkedList<ProcedimientoDTO>();
		try {
			em.getTransaction().begin();
			Tecnico t = em
					.createQuery("from TecnicoImpl t where t.nombre = :nombre",
							TecnicoImpl.class)
					.setParameter("nombre", nombreTecnico).getSingleResult();
			List<Procedimiento> procedimientos = t.getProcedimientos();
			for (Procedimiento p : procedimientos) {
				result.add(new ProcedimientoDTO(p.getCodigo(), p.getNombre(), p
						.getDescripcion()));
			}
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return result;
	}

	public List<ProcedimientoDTO> findAllProdDTOs(String codigo) {
		List<ProcedimientoDTO> result = new LinkedList<ProcedimientoDTO>();
		try {
			em.getTransaction().begin();
			Producto t = em
					.createQuery("from ProductoImpl t where t.codigo = :codigo",
							ProductoImpl.class)
					.setParameter("codigo", codigo).getSingleResult();
			List<Procedimiento> procedimientos = t.getProcedimientos();
			for (Procedimiento p : procedimientos) {
				result.add(new ProcedimientoDTO(p.getCodigo(), p.getNombre(), p
						.getDescripcion()));
			}
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return result;
	}
}
