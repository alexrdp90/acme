package isw2.infrastructure;

import isw2.domain.incidencia.Incidencia;
import isw2.domain.incidencia.IncidenciaImpl;
import isw2.domain.procedimiento.Procedimiento;
import isw2.domain.tecnico.Tecnico;
import isw2.domain.tecnico.TecnicoImpl;
import isw2.presentation.shared.IncidenciaDTO;

import java.util.*;

import javax.persistence.EntityManager;

public class IncidenciaRepository extends JPARepository {

	public void store(Incidencia incidencia) {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(incidencia);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
	}

	public Incidencia find(Integer id) {
		EntityManager em = getEntityManager();
		Incidencia incidencia = new IncidenciaImpl();
		try {
			em.getTransaction().begin();
			incidencia = em.find(IncidenciaImpl.class, id);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return incidencia;
	}

	public List<Incidencia> findAll() {
		EntityManager em = getEntityManager();
		List<Incidencia> result = new LinkedList<Incidencia>();
		try {
			em.getTransaction().begin();
			result.addAll(em.createQuery("from IncidenciaImpl",
					IncidenciaImpl.class).getResultList());
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return result;
	}

	public List<IncidenciaDTO> findAll(Integer idTecnico) {
		EntityManager em = getEntityManager();
		List<IncidenciaDTO> result = new LinkedList<IncidenciaDTO>();
		try {
			em.getTransaction().begin();

			Tecnico t = em.find(TecnicoImpl.class, idTecnico);
			List<String> procCodigos = new LinkedList<String>();
			for (Procedimiento proc : t.getProcedimientos())
				procCodigos.add(proc.getCodigo());

			List<IncidenciaImpl> incidencias = em.createQuery(
					"from IncidenciaImpl", IncidenciaImpl.class)
					.getResultList();
			for (IncidenciaImpl inc : incidencias) {
				if (procCodigos.contains(inc.getProcedimiento().getCodigo())) {
					IncidenciaDTO incidenciaDTO = new IncidenciaDTO();
					incidenciaDTO.setId(inc.getId());
					incidenciaDTO.setCliente(inc.getCliente());
					incidenciaDTO.setEmail(inc.getEmail());
					incidenciaDTO.setDescripcion(inc.getDescripcion());
					incidenciaDTO.setProcCodigo(inc.getProcedimiento()
							.getCodigo());
					result.add(incidenciaDTO);
				}
			}

			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return result;
	}

	public List<IncidenciaDTO> findAllAvailables(Integer idTecnico) {
		EntityManager em = getEntityManager();
		List<IncidenciaDTO> result = new LinkedList<IncidenciaDTO>();
		try {
			em.getTransaction().begin();
			Tecnico t = em.find(TecnicoImpl.class, idTecnico);
			List<Incidencia> incidencias = t.getIncidencias();
			for (Incidencia inc : incidencias) {
				IncidenciaDTO incidenciaDTO = new IncidenciaDTO();
				incidenciaDTO.setId(inc.getId());
				incidenciaDTO.setCliente(inc.getCliente());
				incidenciaDTO.setEmail(inc.getEmail());
				incidenciaDTO.setDescripcion(inc.getDescripcion());
				incidenciaDTO.setProcCodigo(inc.getProcedimiento().getCodigo());
				result.add(incidenciaDTO);
			}
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return result;
	}

	public void update(Incidencia incidencia) {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			em.detach(incidencia);
			em.merge(incidencia);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
	}
}
