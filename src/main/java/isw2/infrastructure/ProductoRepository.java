package isw2.infrastructure;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;
import javax.persistence.EntityManager;
import isw2.domain.producto.*;
import isw2.presentation.shared.ProductoDTO;

public class ProductoRepository extends JPARepository {

	public void store(Producto producto) {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(producto);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
	}

	public Producto find(String id) {
		EntityManager em = getEntityManager();
		Producto producto = new ProductoImpl();
		try {
			em.getTransaction().begin();
			producto = em.find(ProductoImpl.class, id);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return producto;
	}

	public List<Producto> findAll() {
		EntityManager em = getEntityManager();
		List<Producto> productos = new LinkedList<Producto>();
		try {
			em.getTransaction().begin();
			productos.addAll(em.createQuery("from ProductoImpl",
					ProductoImpl.class).getResultList());
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return productos;
	}

	public List<ProductoDTO> findAllDTOs() {
		List<Producto> productos = findAll();
		List<ProductoDTO> result = new ArrayList<ProductoDTO>();
		for (Producto p : productos) {
			result.add(new ProductoDTO(p.getCodigo(), p.getNombre(), p
					.getDescripcion()));
		}
		return result;
	}

	public void update(Producto producto) {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			em.detach(producto);
			em.merge(producto);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
	}

}
