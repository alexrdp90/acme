package isw2.infrastructure;

import java.util.ArrayList;
import java.util.List;

import isw2.domain.procedimiento.Procedimiento;
import isw2.domain.tecnico.Tecnico;
import isw2.domain.tecnico.TecnicoImpl;
import isw2.presentation.shared.TecnicoDTO;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

public class TecnicoDAO {

	private EntityManager em;
	
	public TecnicoDAO(EntityManager em){
		this.em = em;
	}
	
	public void añadirTecnico(Tecnico tecnico){
		em.persist(tecnico);
	}
	
	public Tecnico obtenerTecnico(Integer id){
		return em.find(TecnicoImpl.class, id);
	}
	
	public Tecnico obtenerTecnicoPorNombre(String nombre){
		Tecnico tecnico;
		try{
		tecnico = (Tecnico) em
				.createQuery("FROM TecnicoImpl t WHERE t.nombre = :nombre").setParameter("nombre", nombre)
				.getSingleResult();
		} catch(NoResultException nre) {
			tecnico = null;
		}
		return tecnico;
	}
	
	public List<TecnicoDTO> listarTecnicos(){
		ArrayList<TecnicoDTO> tecnicos = new ArrayList<TecnicoDTO>();
		String querySql = "SELECT new isw2.presentation.shared.TecnicoDTO(t.nombre, t.telefono, t.visible) from TecnicoImpl t";
		tecnicos.addAll(em.createQuery(querySql, TecnicoDTO.class).getResultList());
		return tecnicos;
	}
	
	public void asignarProcedimiento(Integer idTecnico,
			Procedimiento procedimiento) {
		Tecnico tecnico = obtenerTecnico(idTecnico);
		tecnico.addProcedimiento(procedimiento);
	}

}
