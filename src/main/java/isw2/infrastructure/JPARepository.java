package isw2.infrastructure;

import isw2.service.tecnico.TecnicoServiceImpl;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public abstract class JPARepository {

	private static EntityManagerFactory emf;

	public EntityManager getEntityManager() {
		if (emf == null) {
			emf = Persistence.createEntityManagerFactory("acme");
			new TecnicoServiceImpl().registrarTecnicoSystem();
		}
		return emf.createEntityManager();
	}

}
