package isw2.infrastructure;

import java.util.*;

import javax.persistence.EntityManager;

import isw2.domain.respuesta.*;

public class RespuestaRepository extends JPARepository {

	public Respuesta find(Long id) {
		EntityManager em = getEntityManager();
		Respuesta respuesta = new RespuestaImpl();
		try {
			em.getTransaction().begin();
			respuesta = em.find(RespuestaImpl.class, id);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return respuesta;
	}

	public List<Respuesta> findAll() {
		EntityManager em = getEntityManager();
		List<Respuesta> result = new LinkedList<Respuesta>();
		try {
			em.getTransaction().begin();
			result.addAll(em
					.createQuery("from respuestas", RespuestaImpl.class)
					.getResultList());
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return result;
	}

	public void store(Respuesta respuesta) {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(respuesta);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
	}

	public void setValoracion(Integer idRespuesta, Integer valoracion) {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			Respuesta respuesta = em.find(RespuestaImpl.class, new Long(
					idRespuesta));
			respuesta.setValoracion(valoracion);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
	}
}
