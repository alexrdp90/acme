package test.incidencia;

import isw2.domain.incidencia.Incidencia;
import isw2.domain.incidencia.IncidenciaImpl;

import java.util.GregorianCalendar;
import java.util.LinkedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import isw2.domain.procedimiento.Procedimiento;
import isw2.domain.procedimiento.ProcedimientoImpl;
import isw2.domain.producto.Producto;
import isw2.domain.producto.ProductoImpl;

public class TestIncidencia {

	private Incidencia incidencia;

	@Before
	public void setUp() {
		Producto producto = new ProductoImpl("codigo", "nombre", "descripcion");
		Procedimiento procedimiento = new ProcedimientoImpl();
		procedimiento.setCodigo("codigo");
		procedimiento.setNombre("nombre");
		procedimiento.setDescripcion("descripcion");		
		incidencia = new IncidenciaImpl("cliente1", "cliente1@gmail.com", "descripcion", new GregorianCalendar(), producto,
				procedimiento, new LinkedList<Incidencia>());
	}

	@After
	public void tearDown() {
		incidencia = null;
	}
	/*
	 * setCliente()
	 * (x != null) && (x != â€œâ€�)				
	 * x == null -> F #IllegalArgumentException
     * x == â€œâ€� -> F #IllegalArgumentException
	 * x == â€œcliente2â€� -> T  
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testSetCliente1() {
		incidencia.setCliente(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetCliente2() {
		incidencia.setCliente("");
	}
	
	@Test
	public void testSetCliente3() {
		incidencia.setCliente("cliente2");
	}
	
	/*
	 * setDescipcion()
	 * (x != null) && (x != â€œâ€�)				
	 * x == null -> F #IllegalArgumentException
     * x == â€œâ€� -> F #IllegalArgumentException
	 * x == â€œdescripcionâ€� -> T  
	 */	
	@Test(expected=IllegalArgumentException.class)
	public void testSetDescripcion1() {
		incidencia.setDescripcion(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetDescripcion2() {
		incidencia.setCliente("");
	}
	
	@Test
	public void testSetDescripcion3() {
		incidencia.setCliente("descripcion");
	}
	
	/*
	 * setEmail()
	 * (x != null) && (x.matches(\"[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})\")" }))				
	 * x == null -> F #IllegalArgumentException
     * x == â€œâ€� -> F #IllegalArgumentException
     * x == â€œcliente AT gmail DOT comâ€� -> F #IllegalArgumentException
	 * x == â€œcliente@gmail.comâ€� -> T  
	 */
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetEmail1() {
		incidencia.setEmail(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetEmail2() {
		incidencia.setEmail("");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetEmail3() {
		incidencia.setEmail("cliente AT gmail DOT com");
	}
	
	@Test
	public void testSetEmail4() {
		incidencia.setEmail("cliente@gmail.com");
	}
	
	/*
	 * setFechaCompra()
	 * (x != null)				
	 * x == null -> F #IllegalArgumentException
	 * x == new GregorianCalendar() -> T  
	 */
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetFechaCompra1() {
		incidencia.setFechaCompra(null);
	}
	
	@Test
	public void testSetFechaCompra2() {
		incidencia.setFechaCompra(new GregorianCalendar());
	}
	
	/*
	 * setProcedimiento()				
	 * x == null -> T
	 * x == new ProcedimientoImpl("procedi01","Reparar 1", "descrip") -> T  
	 */
	
	@Test
	public void testSetProcedimiento1() {
		incidencia.setProcedimiento(null);
	}
	
	@Test
	public void testSetProcedimiento2() {
		Procedimiento procedimiento = new ProcedimientoImpl();
		procedimiento.setCodigo("procedi01");
		procedimiento.setNombre("Reparar 1");
		procedimiento.setDescripcion("desc2");		
		incidencia.setProcedimiento(procedimiento);
	}
	
}
