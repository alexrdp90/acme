package test.procedimiento;

import org.junit.Before;
import org.junit.Test;

import isw2.domain.procedimiento.*;

public class TestProcedimiento {

	private Procedimiento procedimiento;

	@Before
	public void setUp() {
		procedimiento = new ProcedimientoImpl();
		procedimiento.setCodigo("codigo");
		procedimiento.setNombre("nombre");
		procedimiento.setDescripcion("descripcion");		
	}

	/*
	 * setCodigo()
	 * (x != null) && (x != â€œâ€�)				
	 * x == null -> F #IllegalArgumentException
     * x == â€œâ€� -> F #IllegalArgumentException
	 * x == â€œproductâ€� -> T  
	 */	
	@Test(expected = IllegalArgumentException.class)
	public void testSetCodigo1() {
		procedimiento.setCodigo(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSetCodigo2() {
		procedimiento.setCodigo("");
	}

	@Test
	public void testSetCodigo3() {
		procedimiento.setCodigo("product");
	}
	
	/*
	 * setDescipcion()
	 * (x != null) && (x != â€œâ€�)				
	 * x == null -> F #IllegalArgumentException
     * x == â€œâ€� -> F #IllegalArgumentException
	 * x == â€œDescripciÃ³n del procedimientoâ€� -> T  
	 */	
	@Test(expected = IllegalArgumentException.class)
	public void testSetDesc1() {
		procedimiento.setDescripcion(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetDesc2() {
		procedimiento.setDescripcion("");
	}

	@Test
	public void testSetDesc3() {
		procedimiento.setDescripcion("DescripciÃ³n del procedimiento");
	}
	
	/*
	 * setNombre()
	 * (x != null) && (x != â€œâ€�)				
	 * x == null -> F #IllegalArgumentException
     * x == â€œâ€� -> F #IllegalArgumentException
	 * x == â€œprocedimiento 2â€� -> T  
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testSetNombre1() {
		procedimiento.setNombre(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetNombre2() {
		procedimiento.setNombre("");
	}

	@Test
	public void testSetNombre3() {
		procedimiento.setNombre("procedimiento 2");
	}
}
