package test.respuesta;

import isw2.domain.incidencia.Incidencia;
import isw2.domain.incidencia.IncidenciaImpl;

import java.util.GregorianCalendar;
import java.util.LinkedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import isw2.domain.procedimiento.Procedimiento;
import isw2.domain.procedimiento.ProcedimientoImpl;
import isw2.domain.producto.Producto;
import isw2.domain.producto.ProductoImpl;
import isw2.domain.respuesta.Respuesta;
import isw2.domain.respuesta.RespuestaImpl;

public class TestRespuesta {

	private Respuesta respuesta;

	@Before
	public void setUp() {
		Producto producto = new ProductoImpl("codigo", "nombre", "descripcion");
		Procedimiento procedimiento = new ProcedimientoImpl();
		procedimiento.setCodigo("codigo22");
		procedimiento.setNombre("nombre22");
		procedimiento.setDescripcion("descripcion22");		
		Incidencia inc = new IncidenciaImpl("cliente1", "cliente1@gmail.com","descripcion", new GregorianCalendar(), producto,
				procedimiento, new LinkedList<Incidencia>());
		respuesta = new RespuestaImpl(inc, "respuesta", 5);
	}

	@After
	public void tearDown() {
		respuesta = null;
	}

	/*
	 * setIncidencia()
	 * (x != null) && (x != â€œâ€�)				
	 * x == null -> F #IllegalArgumentException
	 * x == new IncidenciaImpl("cliente2", "cliente2@gmail.com","", new ProcedimientoImpl("codigo", "nombre", "descripcion"), new GregorianCalendar()); -> T  
	 */
	
	@Test(expected = IllegalArgumentException.class)
	public void testSetIncidencia1() {
		respuesta.setIncidencia(null);
	}

	@Test
	public void testSetIncidencia2() {
		Producto producto = new ProductoImpl("codigo", "nombre", "descripcion");
		Procedimiento procedimiento = new ProcedimientoImpl();
		procedimiento.setCodigo("codigo22");
		procedimiento.setNombre("nombre22");
		procedimiento.setDescripcion("descripcion22");		
		Incidencia inc = new IncidenciaImpl("cliente2", "cliente2@gmail.com",
				"descripcion",  new GregorianCalendar(), producto, procedimiento,new LinkedList<Incidencia>());
		respuesta.setIncidencia(inc);
	}
	/*
	 * setDescripcion()
	 * (x != null) && (x != â€œâ€�)				
	 * x == null -> F #IllegalArgumentException
     * x == â€œâ€� -> F #IllegalArgumentException
	 * x == â€œDescripciÃ³n del productoâ€� -> T  
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testSetDescripcion1() {
		respuesta.setDescripcion(null);
	}

	@Test
	public void testSetDescripcion2() {
		respuesta.setDescripcion("");
	}

	@Test
	public void testSetDescripcion3() {
		respuesta.setDescripcion("descripcion");
	}

	/*
	 * setValoracion()
	 * (x != null) && (x != â€œâ€�)				
	 * x == null -> F #IllegalArgumentException
     * x == Integer.MAX_VALUE - 1 -> F #IllegalArgumentException
     * x == Integer.MAX_VALUE  -> F #IllegalArgumentException
     * x == Integer.MIN_VALUE  -> F #IllegalArgumentException
     * x == Integer.MIN_VALUE + 1 -> F #IllegalArgumentException
     * x == -6 -> F #IllegalArgumentException
	 * x == -5 -> T
	 * x == -4 -> T 
     * x == 6 -> F #IllegalArgumentException
	 * x == 5 -> T
	 * x == 4 -> T    
	 */
	
	@Test(expected = IllegalArgumentException.class)
	public void testSetValoracion0() {
		respuesta.setValoracion(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSetValoracion1() {
		respuesta.setValoracion(Integer.MAX_VALUE);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetValoracion2() {
		respuesta.setValoracion(Integer.MAX_VALUE - 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetValoracion3() {
		respuesta.setValoracion(Integer.MIN_VALUE);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetValoracion4() {
		respuesta.setValoracion(Integer.MAX_VALUE + 1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSetValoracion5() {
		respuesta.setValoracion(-6);
	}
	
	@Test
	public void testSetValoracion6() {
		respuesta.setValoracion(-5);
	}
	
	@Test
	public void testSetValoracion7() {
		respuesta.setValoracion(-4);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSetValoracion8() {
		respuesta.setValoracion(6);
	}
	
	@Test
	public void testSetValoracion9() {
		respuesta.setValoracion(5);
	}
	
	@Test
	public void testSetValoracion10() {
		respuesta.setValoracion(4);
	}

}