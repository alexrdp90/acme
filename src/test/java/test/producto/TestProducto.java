package test.producto;

import org.junit.Before;
import org.junit.Test;
import isw2.domain.procedimiento.Procedimiento;
import isw2.domain.procedimiento.ProcedimientoImpl;
import isw2.domain.producto.Producto;
import isw2.domain.producto.ProductoImpl;

public class TestProducto {

	private Producto producto;
	@Before
	public void setUp() {
		producto = new ProductoImpl("prod01","Producto 1", "descrip");
	}
	
	/*
	 * addProcedimiento()
	 * (x != null)
	 * x == null -> F #IllegalArgumentException
     * x == new ProcedimientoImpl("procedi01","Reparar 1", "descrip"); -> T
     *   
	 */	
	
	@Test(expected=IllegalArgumentException.class)
	public void testAddProcedimiento1() {
		producto.addProcedimiento(null);
	}
	
	@Test
	public void testAddProcedimiento2() {
		Procedimiento procedimiento = new ProcedimientoImpl();
		procedimiento.setCodigo("codigo");
		procedimiento.setNombre("nombre");
		procedimiento.setDescripcion("descripcion");		;
		producto.addProcedimiento(procedimiento);
	}
	
	/*
	 * removeProcedimiento()
	 * (x != null) 			
	 * x == null -> F #IllegalArgumentException
     * x == new ProcedimientoImpl("procedi01","Reparar 1", "descrip"); -> T  
	 */	
	
	@Test(expected=IllegalArgumentException.class)
	public void testRemoveProcedimiento1() {
		producto.removeProcedimiento(null);
	}
	
	@Test
	public void testRemoveProcedimiento2() {
		Procedimiento procedimiento = new ProcedimientoImpl();
		procedimiento.setCodigo("codigo22");
		procedimiento.setNombre("nombre2");
		procedimiento.setDescripcion("descripcion22");
		producto.removeProcedimiento(procedimiento);
	}
	
	/*
	 * setCodigo()
	 * (x != null) && (x != â€œâ€�)				
	 * x == null -> F #IllegalArgumentException
     * x == â€œâ€� -> F #IllegalArgumentException
	 * x == â€œprocedimiento 2â€� -> T  
	 */	
	@Test(expected=IllegalArgumentException.class)
	public void testSetCodigo1() {
		producto.setCodigo(null);
	}
	
	@Test
	public void testSetCodigo() {
		producto.setCodigo("product");
	}
	
	/*
	 * setDescripcion()
	 * (x != null) && (x != â€œâ€�)				
	 * x == null -> F #IllegalArgumentException
     * x == â€œâ€� -> F #IllegalArgumentException
	 * x == â€œDescripciÃ³n del productoâ€� -> T  
	 */

	@Test(expected=IllegalArgumentException.class)
	public void testSetDesc1() {
		producto.setDescripcion(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetDesc2() {
		producto.setDescripcion("");
	}
	
	@Test
	public void testSetDesc3() {
		producto.setDescripcion("DescripciÃ³n del producto");
	}
	
	/*
	 * setNombre()
	 * (x != null) && (x != â€œâ€�)				
	 * x == null -> F #IllegalArgumentException
     * x == â€œâ€� -> F #IllegalArgumentException
	 * x == â€œproducto 2â€� -> T  
	 */
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetNombre1() {
		producto.setNombre(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetNombre2() {
		producto.setNombre("");
	}
	
	@Test
	public void testSetNombre3() {
		producto.setNombre("producto 2");
	}

	/*
	 * setVisibilidad()
	 * (x != null)
	 * x == null -> F #IllegalArgumentException
	 * x == â€œtrueâ€� -> T  
	 * x == â€œfalseâ€� -> T  
	 */
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetVisibilidad1() {
		producto.setVisiblilidad(null);
	}

	@Test
	public void testSetVisibilidad2() {
		producto.setVisiblilidad(true);
	}
	
	@Test
	public void testSetVisibilidad3() {
		producto.setVisiblilidad(false);
	}

}
