package test.tecnico;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import isw2.domain.procedimiento.Procedimiento;
import isw2.domain.procedimiento.ProcedimientoImpl;
import isw2.domain.tecnico.Tecnico;
import isw2.domain.tecnico.TecnicoImpl;

public class TestTecnico {

	private Tecnico tecnico;

	@Before
	public void setUp() {
		tecnico = new TecnicoImpl("tecnico1", "pass1", "987654321");
	}

	@After
	public void tearDown() {
		tecnico = null;
	}

	/*
	 * addProcedimiento() (x != null) x == null -> F #IllegalArgumentException x
	 * == new ProcedimientoImpl("procedi01","Reparar 1", "descrip"); -> T
	 */

	@Test(expected = IllegalArgumentException.class)
	public void testAddProcedimiento1() {
		tecnico.addProcedimiento(null);
	}

	@Test
	public void testAddProcedimiento2() {
		Procedimiento procedimiento = new ProcedimientoImpl();
		procedimiento.setCodigo("codigo");
		procedimiento.setNombre("nombre");
		procedimiento.setDescripcion("descripcion");		
		tecnico.addProcedimiento(procedimiento);
	}

	/*
	 * removeProcedimiento() (x != null) x == null -> F
	 * #IllegalArgumentException x == new ProcedimientoImpl("codigo1",
	 * "nombre1", "decripcion"); -> T
	 */

	@Test(expected = IllegalArgumentException.class)
	public void testRemoveProcedimiento1() {
		tecnico.removeProcedimiento(null);
	}

	@Test
	public void testRemoveProcedimiento2() {
		Procedimiento procedimiento = new ProcedimientoImpl();
		procedimiento.setCodigo("codigo");
		procedimiento.setNombre("nombre");
		procedimiento.setDescripcion("descripcion");
		tecnico.removeProcedimiento(procedimiento);
	}

	/*
	 * setNombre() (x != null) && (x != â€œâ€�) x == null -> F
	 * #IllegalArgumentException x == â€œâ€� -> F #IllegalArgumentException x ==
	 * â€œtecnicoâ€� -> T
	 */

	@Test(expected = IllegalArgumentException.class)
	public void testSetNombre1() {
		tecnico.setNombre(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetNombre2() {
		tecnico.setNombre("");
	}

	@Test
	public void testSetNombre3() {
		tecnico.setNombre("tecnico");
	}

	/*
	 * setPassword() (x != null) && (x != â€œâ€�) x == null -> F
	 * #IllegalArgumentException x == â€œâ€� -> F #IllegalArgumentException x ==
	 * â€œpassâ€� -> T
	 */

	@Test(expected = IllegalArgumentException.class)
	public void testSetPassword1() {
		tecnico.setPassword(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetPassword2() {
		tecnico.setPassword("");
	}

	@Test
	public void testSetPassword3() {
		tecnico.setPassword("pass");
	}

	/*
	 * setTelefono() (x != null) && (x != â€œâ€�) x == null -> F
	 * #IllegalArgumentException x == â€œâ€� -> F #IllegalArgumentException x ==
	 * â€œ9876543210â€� -> F #IllegalArgumentException x == â€œ987654321â€� -> T
	 */

	@Test(expected = IllegalArgumentException.class)
	public void testSetTelefono1() {
		tecnico.setTelefono(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetTelefono2() {
		tecnico.setTelefono("");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetTelefono3() {
		tecnico.setTelefono("9876543210");
	}

	@Test
	public void testSetTelefono4() {
		tecnico.setTelefono("987654321");
	}

	/*
	 * setVisibilidad() (x != null) x == null -> F #IllegalArgumentException x
	 * == â€œtrueâ€� -> T x == â€œfalseâ€� -> T
	 */

	@Test(expected = IllegalArgumentException.class)
	public void testSetVisibilidad1() {
		tecnico.setVisiblilidad(null);
	}

	@Test
	public void testSetVisibilidad2() {
		tecnico.setVisiblilidad(true);
	}

	@Test
	public void testSetVisibilidad3() {
		tecnico.setVisiblilidad(false);
	}

}
