package test.app;

import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import isw2.domain.incidencia.Incidencia;
import isw2.domain.procedimiento.Procedimiento;
import isw2.domain.tecnico.Tecnico;
import isw2.service.incidencia.IncidenciaServiceImpl;
import isw2.service.procedimiento.ProcedimientoServiceImpl;
import isw2.service.producto.ProductoServiceImpl;
import isw2.service.tecnico.TecnicoServiceImpl;

import org.junit.Before;
import org.junit.Test;

public class ServicesTests {

	private IncidenciaServiceImpl incidenciaService;
	private ProcedimientoServiceImpl procedimientoService;
	private ProductoServiceImpl productoService;
	private TecnicoServiceImpl tecnicoService;

	@Before
	public void setUp() {
		incidenciaService = new IncidenciaServiceImpl();
		procedimientoService = new ProcedimientoServiceImpl();
		productoService = new ProductoServiceImpl();
		tecnicoService = new TecnicoServiceImpl();
	}

	@Test
	public void procProdTest() {

		procedimientoService.añadirProcedimiento("WC1", "WC1", "WC1");
		procedimientoService.añadirProcedimiento("WC2", "WC2", "WC2");
		procedimientoService.añadirProcedimiento("WC3", "WC3", "WC3");

		productoService.darDeAltaProducto("Web Cam", "Web Cam",
				"Descripcion de Web Cam");

		productoService.asignarProcedimiento("Web Cam", "WC3");
		productoService.asignarProcedimiento("Web Cam", "WC3");
		productoService.asignarProcedimiento("Web Cam", "WC3");

		procedimientoService.añadirProcedimiento("PD1", "PD1", "PD1");
		productoService.darDeAltaProducto("P16GB", "Pen drive 16GB",
				"Descripcion de Pen drive 16GB");
		productoService.asignarProcedimiento("P16GB", "PD1");

		productoService.darDeAltaProducto("P32GB", "Pen drive 32GB",
				"Descripcion de Pen drive 32GB");
		productoService.asignarProcedimiento("P32GB", "PD1");

		procedimientoService.añadirProcedimiento("M1", "M1", "M1");
		procedimientoService.añadirProcedimiento("M2", "M2", "M2");
		procedimientoService.añadirProcedimiento("M3", "M3", "M3");
		procedimientoService.añadirProcedimiento("M4", "M4", "M4");

		productoService.darDeAltaProducto("M24in", "Monitor 24in",
				"Descripcion de Monitor 24in");
		productoService.asignarProcedimiento("M24in", "M1");
		productoService.asignarProcedimiento("M24in", "M2");
		productoService.asignarProcedimiento("M24in", "M3");
		productoService.asignarProcedimiento("M24in", "M4");

		productoService.darDeAltaProducto("M32in", "Monitor 32in",
				"Descripcion de Monitor 32in");
		productoService.asignarProcedimiento("M32in", "M1");
		productoService.asignarProcedimiento("M32in", "M2");
		productoService.asignarProcedimiento("M32in", "M3");
	}

	@Test
	public void tecProcedimientoTest() {

		Tecnico tecnico1 = tecnicoService.añadirTecnico("tecnico1", "tecnico1", "987654321", new LinkedList<String>());
		Tecnico tecnico2 = tecnicoService.añadirTecnico("tecnico2", "tecnico2", "987654321", new LinkedList<String>());
		Tecnico tecnico3 = tecnicoService.añadirTecnico("tecnico3", "tecnico3", "987654321", new LinkedList<String>());

		tecnicoService.asignarProcedimiento(tecnico1.getId(), "WC1");
		tecnicoService.asignarProcedimiento(tecnico1.getId(), "WC2");
		tecnicoService.asignarProcedimiento(tecnico1.getId(), "WC3");

		tecnicoService.asignarProcedimiento(tecnico2.getId(), "WC1");
		tecnicoService.asignarProcedimiento(tecnico2.getId(), "WC2");
		tecnicoService.asignarProcedimiento(tecnico2.getId(), "WC3");
		tecnicoService.asignarProcedimiento(tecnico2.getId(), "PD1");

		for (Procedimiento p : procedimientoService.listarProcedimientos()) {
			tecnicoService.asignarProcedimiento(tecnico3.getId(), p.getCodigo());
		}

	}

	@Test
	public void incidenciasTest() {
		incidenciaService.registrarIncidencia("benito", "benito@gmail.com",
				"Descripcion", new GregorianCalendar(), "Web Cam", "WC1",
				new LinkedList<Incidencia>());

		incidenciaService.registrarIncidencia("benito", "benito@gmail.com",
				"Descripcion", new GregorianCalendar(), "Web Cam", "WC1",
				new LinkedList<Incidencia>());

		incidenciaService.registrarIncidencia("benito", "benito@gmail.com",
				"Descripcion", new GregorianCalendar(), "P32GB", "PD1",
				new LinkedList<Incidencia>());

		incidenciaService.registrarIncidencia("benito", "benito@gmail.com",
				"Descripcion", new GregorianCalendar(), "M24in", "M1",
				new LinkedList<Incidencia>());

		incidenciaService.registrarIncidencia("benito", "benito@gmail.com",
				"Descripcion", new GregorianCalendar(), "M32in", null,
				new LinkedList<Incidencia>());

		Tecnico tecnico1 = tecnicoService.seleccionarTecnicoPorNombre("tecnico1");
		Tecnico tecnico2 = tecnicoService.seleccionarTecnicoPorNombre("tecnico2");
		
		tecnicoService.reclamarIncidencia(tecnico1.getId(), 1);
		tecnicoService.reclamarIncidencia(tecnico1.getId(), 2);
		tecnicoService.reclamarIncidencia(tecnico2.getId(), 3);

		incidenciaService.resolver(1, "Solucionado");
		incidenciaService.valorarRespuesta(1, -5);

		List<Incidencia> incidenciasRel = new LinkedList<Incidencia>();
		incidenciasRel.add(incidenciaService.seleccionarIncidencia(1));
		incidenciaService.registrarIncidencia("benito", "benito@gmail.com",
				"Descripcion", new GregorianCalendar(), "Web Cam", "WC1",
				incidenciasRel);

		tecnicoService.reclamarIncidencia(tecnico1.getId(), 6);
		incidenciaService.resolver(6, "Solucionado");
		incidenciaService.resolver(2, "Solucionado");

	}

}
